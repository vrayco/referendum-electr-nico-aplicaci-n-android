package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import me.rayco.referendumelectronico.model.EventoElectoral;

/**
 * Created by rayco on 17/6/16.
 */
public class APISistemaElectoral {

    Context context = null;

    public APISistemaElectoral(Context _context) {
        context = _context;
    }

    public Boolean enviarVoto(String voto) {
        //instantiates httpclient to make request
        MyHttpClientSistemaElectoral httpclient = new MyHttpClientSistemaElectoral(context);

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_SE_ENVIAR_VOTO);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("m",voto);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = null;
        try {
            se = new StringEntity(jsonobj.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
        httpost.setEntity(se);

        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpclient.execute(httpost);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        if(httpResponse.getStatusLine().getStatusCode() == 200) {
            Log.i(ConstantClass.TAG, "ENVIAR VOTO RESPONSE 200");
            return true;
        }
        else if(httpResponse.getStatusLine().getStatusCode() == 400) {
            Log.i(ConstantClass.TAG, "ENVIAR VOTO RESPONSE 400");
        }
        else if(httpResponse.getStatusLine().getStatusCode() == 500) {
            Log.i(ConstantClass.TAG, "ENVIAR VOTO RESPONSE 500");
        }

        Log.i(ConstantClass.TAG, "* ENVIAR VOTO RESPONSE "+httpResponse.getStatusLine().getStatusCode());

        return false;
    }

}
