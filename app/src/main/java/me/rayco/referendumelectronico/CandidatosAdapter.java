package me.rayco.referendumelectronico;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.EventoElectoral;

public class CandidatosAdapter extends RecyclerView.Adapter<CandidatosAdapter.ViewHolder> {
    private List<Candidato> candidatos;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Candidato candidato;
        public ImageView foto;
        public TextView nombre;
        public TextView seccionLista;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            foto = (ImageView) v.findViewById(R.id.candidato_foto);
            nombre = (TextView) v.findViewById(R.id.candidato_nombre);
            seccionLista = (TextView) v.findViewById(R.id.candidato_seccion_lista);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "onClick " + getPosition() + " Candidato: " + candidato.nombreCompleto);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CandidatosAdapter(List<Candidato> _candidatos) {
        candidatos = _candidatos;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CandidatosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.candidato_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mView.setText(mDataset[position]);
        Candidato c = candidatos.get(position);
        Log.i(ConstantClass.TAG, "CANDIDATO: " + c.toString());
        holder.candidato = c;
        if(c.fotoURL != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + c.fotoURL, holder.foto);
        } else {
            holder.foto.setImageResource(R.drawable.candidato);
        }

        holder.seccionLista.setText(c.seccionLista.toUpperCase());
        holder.nombre.setText(c.posicion + ". " + c.nombreCompleto);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return candidatos.size();
    }
}