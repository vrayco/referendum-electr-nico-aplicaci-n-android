package me.rayco.referendumelectronico;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.rayco.referendumelectronico.model.Candidato;

public class CandidatosSeleccionableAdapter extends RecyclerView.Adapter<CandidatosSeleccionableAdapter.ViewHolder> {
    private List<Candidato> candidatos;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Candidato candidato;
        public ImageView foto;
        public CheckBox nombre;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            foto = (ImageView) v.findViewById(R.id.candidato_seleccionable_foto);
            nombre = (CheckBox) v.findViewById(R.id.candidato_seleccionable_checkbox);
        }

        @Override
        public void onClick(View v) {
            if(this.candidato.seleccionado) {
                this.candidato.seleccionado = false;
                this.nombre.setChecked(false);
                this.candidato.save();
            } else {
                if(candidato.lista.votacionListasAbiertas.getNumeroSeleccionados(candidato.lista.votacionListasAbiertas) < candidato.lista.votacionListasAbiertas.votosPorElector)  {
                    candidato.seleccionado = true;
                    this.nombre.setChecked(true);
                    candidato.save();
                } else {
                    Toast.makeText(v.getContext(), "No puedes seleccionar más candidatos",
                            Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CandidatosSeleccionableAdapter(List<Candidato> _candidatos) {
        candidatos = _candidatos;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CandidatosSeleccionableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                        int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.candidato_seleccionable_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mView.setText(mDataset[position]);
        Candidato c = candidatos.get(position);
        holder.candidato = c;
        if(c.fotoURL != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + c.fotoURL, holder.foto);
        } else {
            holder.foto.setImageResource(R.drawable.candidato);
        }

        holder.nombre.setText(c.nombreCompleto);
        if(c.seleccionado)
            holder.nombre.setChecked(true);
        else
            holder.nombre.setChecked(false);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return candidatos.size();
    }
}