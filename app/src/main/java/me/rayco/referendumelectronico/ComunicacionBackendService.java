package me.rayco.referendumelectronico;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ComunicacionBackendService extends IntentService {

    public ComunicacionBackendService() {
        super("ComunicacionBackendService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ConstantClass.LAB_ACTION_SEND.equals(action)) {
                final String param1 = intent.getStringExtra(ConstantClass.EXTRA_JSON_DATA);
                final String param2 = intent.getStringExtra(ConstantClass.EXTRA_URL);
                try {
                    JSONObject result = handleActionSend(param1, param2);
                    Log.i(ConstantClass.TAG, "Resultado envio: " + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private JSONObject handleActionSend(String data, String url) throws IOException {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(url);

        StringEntity se = null;
        try {
            se = new StringEntity(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httpost.setEntity(se);

        //sets a request header so the page receving the request
        //will know what to do with it
        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        JSONObject content = null;
        try {
            String resp_body = EntityUtils.toString(httpResponse.getEntity());
            content = new JSONObject(resp_body);
        } catch (JSONException e) {
            Log.i(ConstantClass.TAG, "Problema creando el JSONObject para la respuesta.");
            e.printStackTrace();
        }

        JSONObject result = new JSONObject();

        try {
            result.put("codigo", httpResponse.getStatusLine().getStatusCode());
            result.put("contenido",content);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


}
