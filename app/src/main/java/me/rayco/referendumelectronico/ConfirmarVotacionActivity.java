package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;


public class ConfirmarVotacionActivity extends ActionBarActivity {

    private Context context = null;
    private EventoElectoral evento = null;
    private JSONObject voto = null;
    private String votoStr = null;

    private ImageView logotipo = null;
    private TextView titulo = null;
    private Button boton = null;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar);

        context = getApplicationContext();

        Intent intent = getIntent();
        Integer id_evento = intent.getIntExtra("id_evento", -1);

        if(id_evento == -1) {
            Toast toast = Toast.makeText(this, "ERROR: El evento ID=" + id_evento + " no existe.", Toast.LENGTH_SHORT);
            toast.show();
            Intent intent2 = new Intent(this, EventosActivity.class);
            this.startActivity(intent2);
        }

        this.evento = EventoElectoral.get(id_evento);

        // logotipo
        this.logotipo = (ImageView) findViewById(R.id.confirmar_logotipo);
        if(this.evento.logotipoURL != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + this.evento.logotipoURL, this.logotipo);
        }

        // titulo
        this.titulo = (TextView) findViewById(R.id.confirmar_titulo);
        this.titulo.setText(this.evento.nombre);

        // boton
        this.boton = (Button) findViewById(R.id.confirmar_votar);

        mRecyclerView = (RecyclerView) findViewById(R.id.confirmar_votaciones_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<VotacionListasCerradas> votacionListasCerradas = this.evento.VotacionesListasCerradas();
        List<VotacionListasAbiertas> votacionListasAbiertas = this.evento.VotacionesListasAbiertas();
        List<VotacionReferendum> referendums = this.evento.VotacionesReferendums();
        mAdapter = new ConfirmarVotacionesAdapter(this, votacionListasCerradas, votacionListasAbiertas, referendums);
        mRecyclerView.setAdapter(mAdapter);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setTitle("ENVIAR VOTO");
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setSubtitle(evento.nombre);
        } else {
            setTitle(evento.nombre);
        }

    }

    public void EnviarVoto(View view)
    {
        boton.setEnabled(false);
        VotoUtils votoUtils = new VotoUtils(context, evento);
        try {
            // Genero el voto
            if(votoUtils.generar()) {
                votoStr = votoUtils.getVotoCifrado();

                // Pongo la app en modo de envio de voto (guardo el id del evento)
                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(ConstantClass.PROPERTY_VOTO_ENVIADO, evento.iden);
                editor.commit();

                new EnviarVotoTask().execute();
            }
            // Paso a esperar por la tarea en la activity Wait
            Context context = getApplicationContext();
            Intent intent = new Intent(context, WaitActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "El proceso electoral no está abierto. No se ha enviado el voto",
                    Toast.LENGTH_SHORT).show();
            boton.setEnabled(true);
        }

    }

    private class EnviarVotoTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.i(ConstantClass.TAG, "Inicio del enviando el voto");

            APISistemaElectoral api = new APISistemaElectoral(context);
            Log.i(ConstantClass.TAG, "***VOTO***: "+ votoStr);

            Boolean resultadoEnvio = api.enviarVoto(votoStr);

            if(resultadoEnvio)
                return "Executed";
            else
                return "Fail";

        }

        @Override
        protected void onPostExecute(String result) {
            Intent intent;
            if(result.equals("Executed")) {
                Log.i(ConstantClass.TAG, "Se está validando tu voto");
                Toast.makeText(context, "Tu voto ha sido enviado",
                        Toast.LENGTH_SHORT).show();
            } else {
                Log.i(ConstantClass.TAG, "No se ha enviado el voto");
                Toast.makeText(context, "El proceso electoral no está abierto. No se ha enviado el voto",
                        Toast.LENGTH_SHORT).show();

                // Elimino el estado de app en modo de envio de voto
                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(ConstantClass.PROPERTY_VOTO_ENVIADO);
                editor.commit();
            }

            intent = new Intent(context, EventoActivity.class);
            intent.putExtra("id_evento", evento.iden);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
