package me.rayco.referendumelectronico;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.Lista;
import me.rayco.referendumelectronico.model.Pregunta;
import me.rayco.referendumelectronico.model.Respuesta;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;

public class ConfirmarVotacionesAdapter extends RecyclerView.Adapter<ConfirmarVotacionesAdapter.ViewHolder> {
    private Context context;
    private List<VotacionListasCerradas> listasCerradas;
    private List<VotacionListasAbiertas> listasAbiertas;
    private List<VotacionReferendum> referendums;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public VotacionListasCerradas votacionCerrada;
        public VotacionListasAbiertas votacionAbierta;
        public VotacionReferendum referendum;
        public String tipoVotacion;

        public TextView preguntasReferendum;
        public TextView firstLine;
        public TextView secondLine;
        public ImageView logotipo;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            Log.i(ConstantClass.TAG, "CONFIRMAR VOTACION ADAPTER");
            preguntasReferendum = (TextView) v.findViewById(R.id.confirmar_referendum);
            firstLine = (TextView) v.findViewById(R.id.confimar_votacion_firstLine);
            secondLine = (TextView) v.findViewById(R.id.confimar_votacion_secondLine);
            logotipo = (ImageView) v.findViewById(R.id.confimar_votacion_iconEstado);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "onClick en votacion " + getPosition());
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ConfirmarVotacionesAdapter(Context _context, List<VotacionListasCerradas> _listasCerradas, List<VotacionListasAbiertas> _listasAbiertas, List<VotacionReferendum> _referendums) {
        this.context = _context;
        this.listasCerradas = _listasCerradas;
        this.listasAbiertas = _listasAbiertas;
        this.referendums = _referendums;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ConfirmarVotacionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.confirmar_votacion_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        int index = -1;
        int tipo = -1;
        if(this.listasAbiertas.size() > 0 && position < this.listasAbiertas.size()) {
            index = position;
            tipo = 1;
        } else if(this.listasCerradas.size() > 0 && (this.listasAbiertas.size() <= position && position < (this.listasAbiertas.size() + this.listasCerradas.size()))) {
            index = position - this.listasAbiertas.size();
            tipo = 2;
        } else {
            index = position - this.listasAbiertas.size() - this.listasCerradas.size();
            tipo = 3;
        }

        switch (tipo) {
            case 1:
                    this.listasAbiertas(holder, index);
                break;
            case 2:
                    this.listasCerradas(holder, index);
                break;
            case 3:
                    this.referendums(holder, index);
                break;
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.listasAbiertas.size() + this.listasCerradas.size() + this.referendums.size();
    }

    private void listasCerradas(ViewHolder holder, int position)
    {
        Log.i(ConstantClass.TAG, "LISTA CERRADA");
        VotacionListasCerradas v2 = this.listasCerradas.get(position);

        List<Lista> listas = v2.ListasSeleccionadas();
        String strListas = "";
        if(listas.size() == 0)
            strListas = "EN BLANCO";
        else
            for(Lista l : listas)
                if(l.seleccionado)
                    strListas += "<strong>" + l.nombreAbreviado + "</strong></br>";


        if(v2.logotipoURL != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + v2.logotipoURL, holder.logotipo);
        }

        holder.votacionCerrada = v2;
        holder.tipoVotacion = "VotacionListasCerradas";
        holder.firstLine.setText(v2.nombre);
        holder.secondLine.setText(Html.fromHtml(strListas));
    }

    private void listasAbiertas(ViewHolder holder, int position)
    {
        Log.i(ConstantClass.TAG, "LISTA ABIERTA");
        VotacionListasAbiertas v2 = this.listasAbiertas.get(position);

        List<Candidato> candidatos = v2.CandidatosSeleccionadas();
        String strCandidatos = "";
        if(candidatos.size() == 0)
            strCandidatos = "EN BLANCO";
        else
            for(Candidato c : candidatos) {
                strCandidatos += "<strong>" + c.nombreCompleto + "</strong>" + " (" + c.lista.nombreAbreviado + ")<br/>";
            }

        if(v2.logotipoURL != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + v2.logotipoURL, holder.logotipo);
        }

        holder.votacionAbierta = v2;
        holder.tipoVotacion = "VotacionListasAbiertas";
        holder.firstLine.setText(v2.nombre);
        holder.secondLine.setText(Html.fromHtml(strCandidatos));
    }

    private void referendums(ViewHolder holder, int position)
    {
        Log.i(ConstantClass.TAG, "REFERENDUM");
        VotacionReferendum v = this.referendums.get(position);

        List<Pregunta> preguntas = v.Preguntas();
        Log.i(ConstantClass.TAG, "PREGUNTAS NUM: " + preguntas.size());
        String strPreguntas = "";
        for(Pregunta p : preguntas)
        {
            List<Respuesta> respuestas = p.respuestasSeleccinadas();
            if(respuestas.size() == 0)
                strPreguntas += p.pregunta + " <strong>EN BLANCO</strong><br>";
            else
                for(Respuesta r: respuestas)
                    strPreguntas += p.pregunta + " <strong>" + r.respuesta + "</strong><br>";
        }

        holder.preguntasReferendum.setVisibility(View.VISIBLE);
        holder.logotipo.setVisibility(View.INVISIBLE);
        holder.firstLine.setVisibility(View.INVISIBLE);
        holder.secondLine.setVisibility(View.INVISIBLE);

        holder.referendum = v;
        holder.tipoVotacion = "VotacionReferendum";
        holder.preguntasReferendum.setText(Html.fromHtml(strPreguntas));
    }
}