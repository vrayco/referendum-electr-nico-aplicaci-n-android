package me.rayco.referendumelectronico;

/**
 * Created by rayco on 3/4/15.
 */
public final class ConstantClass {

    public static final String API_URL_SISTEMA_GENERAL    = "https://www.referendum-electronico.com/app.php";
    public static final String API_URL_SISTEMA_ELECTORAL  = "https://52.39.90.251/app.php";
//    public static final String API_URL_SISTEMA_GENERAL    = "https://www.referendum-electronico.com/app_dev.php";
//    public static final String API_URL_SISTEMA_ELECTORAL  = "https://52.39.90.251/app_dev.php";

    public static final String RESOURCES_URL              = "https://www.referendum-electronico.com";

    // CONSULTAS SISTEMA GENERAL
    public static final String API_SG_CLAVE_PUBLICA       = API_URL_SISTEMA_GENERAL + "/api/public/v1/clave-publica/servidor.json";
    public static final String API_INICIAR_SOLICITUD      = API_URL_SISTEMA_GENERAL + "/api/public/v1/solicitud/app.json";
    public static final String API_VALIDAR_CODIGO1        = API_URL_SISTEMA_GENERAL + "/api/public/v1/solicitud/app/codigo1.json";
    public static final String API_VALIDAR_CODIGO2        = API_URL_SISTEMA_GENERAL + "/api/public/v1/solicitud/app/codigo2.json";
    public static final String API_VALIDACION_USUARIO     = API_URL_SISTEMA_GENERAL + "/api/public/v1/validacion/usuario/%s/estado.json";
    public static final String API_VALIDACION_APP         = API_URL_SISTEMA_GENERAL + "/api/public/v1/validacion/app/%s/%s/estado.json";
    public static final String API_ENTREGAR_CLAVE_PUBLICA = API_URL_SISTEMA_GENERAL + "/api/public/v1/clave-publica/entregar.json";
    public static final String API_CONFIRMAR_CLAVE_PUBLICA= API_URL_SISTEMA_GENERAL + "/api/public/v1/clave-publica/confirmar.json";
    public static final String API_GET_CONVOCATORIAS      = API_URL_SISTEMA_GENERAL + "/api/public/v1/evento-electoral/convocatorias/%s/electorales.json";
    public static final String API_GET_EVENTO             = API_URL_SISTEMA_GENERAL + "/api/public/v1/evento-electoral/evento/%s/%s/electoral.json";
    public static final String API_VALIDAR_VOTO           = API_URL_SISTEMA_GENERAL + "/api/public/v1/validacion/voto.json";

    // CONSULTAS SISTEMA ELECTORAL
    public static final String API_SE_CLAVE_PUBLICA       = API_URL_SISTEMA_ELECTORAL + "/api/public/v1/clave-publica/servidor.json";
    public static final String API_SE_ENVIAR_VOTO         = API_URL_SISTEMA_ELECTORAL + "/api/public/v1/voto.json";

    // URL lab
    public static final String LAB_ENVIAR_MENSAJE_CIFRADO = API_URL_SISTEMA_GENERAL + "/api/lab/mensaje_cifrado.json";
    public static final String LAB_ENVIAR_MENSAJE_CIFRADO2= API_URL_SISTEMA_GENERAL + "/api/lab/mensaje_cifrado2.json";
    public static final String LAB_ACTION_SEND = "me.rayco.referendumelectronico.action.SEND_TO_BACKEND";


    public static final String EXTRA_JSON_DATA          = "me.rayco.referendumelectronico.extra.JSON_DATA";
    public static final String EXTRA_URL                = "me.rayco.referendumelectronico.extra.URL";
    public static final String EXTRA_CODIGO_VALIDACION  = "me.rayco.referendumelectronico.extra.EXTRA_CODIGO_VALIDACION";
    public static final String EXTRA_L1                 = "me.rayco.referendumelectronico.extra.EXTRA_L1";

    public static final String SENDER_ID = "508629544010";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String SHARED_PREFERENCES_NAME = "referendum-electronico_app";

    // Solicitud de validación
    public static final String PROPERTY_SOLICITUD_REGISTRO_FASE_1       = "solicitud_registro_fase_1";
    public static final String PROPERTY_SOLICITUD_REGISTRO_FASE_2       = "solicitud_registro_fase_2";
    public static final String PROPERTY_VALIDACION_COMPLETADA           = "validacion_completada";
    public static final String PROPERTY_ENTREGA_CLAVE_PUBLICA_ABIERTO   = "entrega_clave_publica_abierto";
    public static final String PROPERTY_CLAVE_PUBLICA_SG                = "clave_publica_sg";
    public static final String PROPERTY_CLAVE_PUBLICA_SE                = "clave_publica_se";
    public static final String PROPERTY_VOTO_ENVIADO                    = "voto_enviado";

    // Datos del usuario
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String PROPERTY_DNI = "dni";

    public static final String PROPERTY_VALUE_EDITTEXTDNI = "valor_edittext_dni";

    public static final String TAG = "referendum-electronico";

    public static final String KEY_STORE = "AndroidKeyStore";
    public static final String RSA_CN = "CN=referendum-electronico.com";
    public static final String APP_KEY_RSA = "app_key_rsa";
    public static final String VOTACION_LISTAS_CERRADAS = "votacion_listas_cerradas";
    public static final String VOTACION_LISTAS_ABIERTAS = "votacion_listas_abiertas";
    public static final String VOTACION_REFERENDUM = "votacion_referendum";

    public static final String ACTION_DEVOLVER_CODIGO1              = "me.rayco.referendumelectronico.action.DEVOLVER_CODIGO_1";
    public static final String ACTION_ENTREGAR_CLAVE_PUBLICA        = "me.rayco.referendumelectronico.action.ENTREGAR_CLAVE_PUBLICA";
    public static final String ACTION_CONFIRMAR_CLAVE_PUBLICA       = "me.rayco.referendumelectronico.action.CONFIRMAR_CLAVE_PUBLICA";
    public static final String ACTION_DOWNLOAD_EVENTO               = "me.rayco.referendumelectronico.action.DOWNLOAD_EVENTO_ELECTORAL";
    public static final String ACTION_GET_CONVOCATORIAS             = "me.rayco.referendumelectronico.action.GET_CONVOCATORIAS_ELECTORALES";
    public static final String ACTION_EVENTO_GUARDADO               = "me.rayco.referendumelectronico.action.ACTION_EVENTO_GUARDADO";
    public static final String ACTION_REFERENDUM_RESPUESTA          = "me.rayco.referendumelectronico.action.ACTION_REFERENDUM_RESPUESTA";
    public static final String ACTION_VALIDAR_VOTO                  = "me.rayco.referendumelectronico.action.ACTION_VALIDAR_VOTO";


    public static final String EXTRA_CODIGO1 = "me.rayco.referendumelectronico.extra.CODIGO_1";
    public static final String EXTRA_CODIGO2 = "me.rayco.referendumelectronico.extra.CODIGO_2";
    public static final String EXTRA_CODIGO_CIFRADO = "me.rayco.referendumelectronico.extra.EXTRA_CODIGO_CIFRADO";
    public static final String EXTRA_MSG = "me.rayco.referendumelectronico.extra.MSG";
    public static final String EXTRA_CLAVE_PUBLICA = "me.rayco.referendumelectronico.extra.CLAVE_PUBLICA";
    public static final String EXTRA_EVENTO_ID = "me.rayco.referendumelectronico.extra.EVENTO_ID";
    public static final String EXTRA_TOKEN = "me.rayco.referendumelectronico.extra.TOKEN";


    public static final String REGEX_TEXT_SMS_CODIGO2 = "(CODIGO REFERENDUM-ELECTRONICO.COM=)(\\w+)";

    public static final String MSG_ERROR_SMS = "El código sms no es correcto.";

    public static final Integer NOTIFICACION_VOTO_VALIDADO      = 001;
    public static final Integer NOTIFICACION_EVENTO_CONVOCADO   = 002;
    public static final Integer NOTIFICACION_EVENTO_ABIERTO     = 003;
    public static final Integer NOTIFICACION_EVENTO_FINALIZADO  = 004;

}
