package me.rayco.referendumelectronico;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;


public class EventoActivity extends ActionBarActivity {

    Integer id_evento = null;
    EventoElectoral evento = null;

    ImageView logotipo      = null;
    TextView titulo         = null;
    TextView descripcion    = null;
    TextView estado         = null;
    TextView votaciones     = null;
    TextView l1             = null;
    TextView l1Cifrado      = null;
    Button iniciarVotacion  = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);

        Intent intent = getIntent();
        this.id_evento = intent.getIntExtra("id_evento", -1);

        if(id_evento == -1) {
            Toast toast = Toast.makeText(this, "ERROR: El evento ID=" + this.id_evento + " no existe.", Toast.LENGTH_SHORT);
            toast.show();
            Intent intent2 = new Intent(this, EventosActivity.class);
            this.startActivity(intent2);
        }

        this.evento = EventoElectoral.get(id_evento);
        if(this.evento == null) {
            Toast toast = Toast.makeText(this, "ERROR: Cargando el evento ID=" + this.id_evento + " no existe.", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            // logotipo
            this.logotipo = (ImageView) findViewById(R.id.evento_logotipo);
            if (this.evento.logotipoURL != null) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + this.evento.logotipoURL, this.logotipo);
            }

            // titulo
            this.titulo = (TextView) findViewById(R.id.evento_titulo);
            this.titulo.setText(this.evento.nombre);

            // Descripcion
            this.descripcion = (TextView) findViewById(R.id.evento_descripcion);
            this.descripcion.setText(this.evento.descripcion);

            // Estado
            this.estado = (TextView) findViewById(R.id.evento_estado);
            this.estado.setText(this.evento.estado);

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

            //Votaciones
            this.votaciones = (TextView) findViewById(R.id.evento_votaciones);
            String strVotaciones = "";
            List<VotacionListasAbiertas> votacionListasAbiertasList = this.evento.VotacionesListasAbiertas();
            for (VotacionListasAbiertas v : votacionListasAbiertasList) {
                //Log.i(ConstantClass.TAG, v.toString());
                strVotaciones += v.nombre + " (Listas Abiertas)\n";
            }

            List<VotacionListasCerradas> votacionListasCerradasList = this.evento.VotacionesListasCerradas();
            for (VotacionListasCerradas v : votacionListasCerradasList) {
                //Log.i(ConstantClass.TAG, v.toString());
                strVotaciones += v.nombre + " (Listas Cerradas)\n";
            }

            List<VotacionReferendum> VotacionReferendumsList = this.evento.VotacionesReferendums();
            for (VotacionReferendum v : VotacionReferendumsList) {
                strVotaciones += v.nombre + " (Referendum)\n";
            }

            this.votaciones.setText(strVotaciones);

            this.iniciarVotacion = (Button) findViewById(R.id.evento_boton_votacion);
            if(evento.estado.equals("FINALIZADO"))
                this.iniciarVotacion.setVisibility(View.INVISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                setTitle(R.string.app_name);
                android.support.v7.app.ActionBar ab = getSupportActionBar();
                ab.setSubtitle(evento.nombre);
            } else {
                setTitle(evento.nombre);
            }
        }
    }

    public void IniciarVotacion(View view) {
        Intent intent3 = new Intent(this, VotacionesActivity.class);
        intent3.putExtra("id_evento", this.id_evento);
        this.startActivity(intent3);
    }

    public void AbrirTablonAnuncios(View view)
    {
        Log.i(ConstantClass.TAG, "url: " +evento.tablonAnunciosUrl);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantClass.RESOURCES_URL + evento.tablonAnunciosUrl));
        startActivity(browserIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_evento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, EventosActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.evento = EventoElectoral.get(id_evento);
        if(evento.votoEmitido != null) {

            this.l1 = (TextView) findViewById(R.id.evento_l1);
            this.l1.setText(this.evento.l1);

            this.l1Cifrado = (TextView) findViewById(R.id.evento_l1_cifrado);
            this.l1Cifrado.setText(this.evento.l1Cifrado);

            findViewById(R.id.evento_block_prueba_votacion).setVisibility(View.VISIBLE);
            findViewById(R.id.evento_block_prueba_votacion_cifrada).setVisibility(View.VISIBLE);
        }

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(ConstantClass.NOTIFICACION_VOTO_VALIDADO);
        notificationManager.cancel(ConstantClass.NOTIFICACION_EVENTO_ABIERTO);
        notificationManager.cancel(ConstantClass.NOTIFICACION_EVENTO_FINALIZADO);
    }
}
