package me.rayco.referendumelectronico;

import android.app.IntentService;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.Lista;
import me.rayco.referendumelectronico.model.Pregunta;
import me.rayco.referendumelectronico.model.Respuesta;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;

/**
 * Created by rayco on 3/4/15.
 */
public class EventoElectoralService extends IntentService {

    Context context;
    String dni;

    public EventoElectoralService() {
        super("EventoElectoralService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(ConstantClass.TAG, "En evento Electoral SERVICE");
        context = getApplicationContext();
        if (intent != null) {
            final SharedPreferences prefs = getPreferences(context);
            String regid = prefs.getString(ConstantClass.PROPERTY_REG_ID, "");
            dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
            final String action = intent.getAction();
            if (ConstantClass.ACTION_DOWNLOAD_EVENTO.equals(action)) {
                final int id = intent.getIntExtra(ConstantClass.EXTRA_EVENTO_ID, -1);
                final String token = intent.getStringExtra(ConstantClass.EXTRA_TOKEN);
                handleActionGetEvento(id, token);
            } else if (ConstantClass.ACTION_GET_CONVOCATORIAS.equals(action)) {
                //final String codigo_cifrado = intent.getStringExtra(ConstantClass.EXTRA_CODIGO_CIFRADO);
                handleActionGetConvocatorias();
            }
//            } else if (ConstantClass.ACTION_ENTREGAR_CLAVE_PUBLICA.equals(action)) {
//
//                handleActionEntregarClavePublica();
//            }
        }
    }

    private void handleActionGetConvocatorias() {

        Log.i(ConstantClass.TAG, "Obtenindo las convocatorias");
        // Obtengo ids de los eventos y los tokens para consulta
        try {
            String data = this.getConvocatorias();
            Log.i(ConstantClass.TAG, "Resultado: " + data);

            try {
                JSONArray array = new JSONArray(data);
                for(int i = 0; i < array.length(); i++) {
                    Log.i(ConstantClass.TAG, "i" + i + ": " + array.get(i).toString());
                    JSONObject item = array.getJSONObject(i);
                    Integer id_evento = item.getInt("id_evento");
                    String token_cifrado = item.getString("token_cifrado");

                    // Descifro el token recibido
                    Rsa rsa = new Rsa();
                    PrivateKey privateKey = rsa.getPrivateKeyAndroid();
                    String token = rsa.privateDecrypt(token_cifrado, privateKey);
                    Log.i(ConstantClass.TAG, "TOKEN descifrado: " + token);

                    // LLamo al servicio para que descargue el evento, si no existe previamente
                    Intent intent = new Intent(getApplicationContext(), EventoElectoralService.class);
                    intent.setAction(ConstantClass.ACTION_DOWNLOAD_EVENTO);
                    intent.putExtra(ConstantClass.EXTRA_EVENTO_ID, id_evento);
                    intent.putExtra(ConstantClass.EXTRA_TOKEN, token);
                    getApplicationContext().startService(intent);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void handleActionGetEvento(Integer id_evento, String token) {
        Log.i(ConstantClass.TAG, "En accion get evento= " + id_evento + " token = " + token);

        try {
            EventoElectoral evento = EventoElectoral.get(id_evento);

            if(evento == null) {
                String data = this.getEvento(token);
                Log.i(ConstantClass.TAG, "RESULTADO DESCARGA EVENTO: " + data.toString());
                try {
                    JSONObject json = new JSONObject(data);
                    this.guardarEvento(json);
                } catch (JSONException e) {
                    Log.i(ConstantClass.TAG, "Excepción 1 descargando evento: " + e.getMessage());
                    e.printStackTrace();
                } catch (ParseException e) {
                    Log.i(ConstantClass.TAG, "Excepción 1 descargando evento");
                    e.printStackTrace();
                }
                //Log.i(ConstantClass.TAG, "Evento " + id_evento + ": " + data);
            } else {
                Log.i(ConstantClass.TAG, "Evento " + id_evento + ": Ya ha sido descargado.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    private void guardarEvento(JSONObject json) throws JSONException, ParseException {
        // Datos del evento
        Log.i(ConstantClass.TAG, "EVENTOOO: " + json.toString());
        EventoElectoral evento = new EventoElectoral();
        evento.iden = json.getJSONObject("evento_electoral").getInt("id");
        evento.nombre = json.getJSONObject("evento_electoral").getString("nombre");
        evento.descripcion = json.getJSONObject("evento_electoral").getString("descripcion");
        evento.estado = json.getJSONObject("evento_electoral").getString("estado");
        if(json.getJSONObject("evento_electoral").isNull("logotipo")) {
            evento.logotipoURL = null;
        } else {
            evento.logotipoURL = json.getJSONObject("evento_electoral").getJSONObject("logotipo").getString("path");
        }
        evento.tablonAnunciosUrl = json.getJSONObject("evento_electoral").getString("url_tablon_anuncios");;

        // Id elector
        evento.id_elector = json.getInt("id_elector");

        // Token votacion
        evento.tokenVotacion = json.getString("token_votacion");

        // Pubkey evento electoral
        evento.pubKey = json.getString("public_key");

        evento.save();
        // Votaciones
        JSONArray json_votaciones = json.getJSONObject("evento_electoral").getJSONArray("votaciones");
        for(int i = 0; i < json_votaciones.length(); i++) {
            //Log.i(ConstantClass.TAG, "Votacion:" + json_votacion.get(i).toString());
            JSONObject votacion = json_votaciones.getJSONObject(i);
            if(votacion.getString("class_name").equals(ConstantClass.VOTACION_LISTAS_CERRADAS)) {
                VotacionListasCerradas entity_votacion = new VotacionListasCerradas();
                entity_votacion.iden = votacion.getInt("id");
                entity_votacion.nombre = votacion.getString("nombre");
                entity_votacion.descripcion = votacion.getString("descripcion");
                if(votacion.isNull("logotipo")) {
                    entity_votacion.logotipoURL = null;
                } else {
                    entity_votacion.logotipoURL = votacion.getJSONObject("logotipo").getString("path");
                }
                entity_votacion.votoEnBlanco = votacion.getBoolean("voto_en_blanco");
                entity_votacion.votosPorElector = votacion.getInt("votos_por_elector");
                entity_votacion.eventoElectoral = evento;

                entity_votacion.save();

                JSONArray listas = votacion.getJSONArray("listas");
                for(int j = 0; j < listas.length(); j++) {
                    JSONObject lista = listas.getJSONObject(j);
                    Lista entity_lista = new Lista();
                    entity_lista.iden = lista.getInt("id");
                    entity_lista.nombreCompleto = lista.getString("nombre_completo");
                    entity_lista.nombreAbreviado = lista.getString("nombre_abreviado");
                    if(!lista.isNull("logotipo"))
                        entity_lista.path_logotipo = lista.getJSONObject("logotipo").getString("path");
                    entity_lista.votacionListasCerradas = entity_votacion;
                    //Log.i(ConstantClass.TAG, "lista: " + entity_lista.toString());
                    entity_lista.save();

                    JSONArray candidatos = lista.getJSONArray("candidatos");
                    for(int k = 0; k < candidatos.length(); k++) {
                        JSONObject candidato = candidatos.getJSONObject(k);
                        Candidato entity_candidato = new Candidato();
                        entity_candidato.iden = candidato.getInt("id");
                        entity_candidato.nombreCompleto = candidato.getString("nombre_completo");
                        if(candidato.isNull("foto")) {
                            entity_candidato.fotoURL = null;
                        } else {
                            entity_candidato.fotoURL = candidato.getJSONObject("foto").getString("path");
                        }
                        entity_candidato.posicion = candidato.getInt("posicion");
                        entity_candidato.seccionLista = candidato.getString("seccion_lista");

                        entity_candidato.lista = entity_lista;

                        //Log.i(ConstantClass.TAG, "Candidato: " + entity_candidato.toString());
                        entity_candidato.save();
                    }
                }

            } else if(votacion.getString("class_name").equals(ConstantClass.VOTACION_LISTAS_ABIERTAS)) {
                VotacionListasAbiertas entity_votacion = new VotacionListasAbiertas();
                entity_votacion.iden = votacion.getInt("id");
                entity_votacion.nombre = votacion.getString("nombre");
                entity_votacion.descripcion = votacion.getString("descripcion");
                if(votacion.isNull("logotipo")) {
                    entity_votacion.logotipoURL = null;
                } else {
                    entity_votacion.logotipoURL = votacion.getJSONObject("logotipo").getString("path");
                }
                entity_votacion.votoEnBlanco = votacion.getBoolean("voto_en_blanco");
                entity_votacion.votosPorElector = votacion.getInt("votos_por_elector");
                entity_votacion.eventoElectoral = evento;

                entity_votacion.save();

                JSONArray listas = votacion.getJSONArray("listas");
                for(int j = 0; j < listas.length(); j++) {
                    JSONObject lista = listas.getJSONObject(j);
                    Lista entity_lista = new Lista();
                    entity_lista.iden = lista.getInt("id");
                    entity_lista.nombreCompleto = lista.getString("nombre_completo");
                    entity_lista.nombreAbreviado = lista.getString("nombre_abreviado");
                    if(!lista.isNull("logotipo"))
                        entity_lista.path_logotipo = lista.getJSONObject("logotipo").getString("path");
                    entity_lista.votacionListasAbiertas = entity_votacion;
                    //Log.i(ConstantClass.TAG, "lista: " + entity_lista.toString());
                    entity_lista.save();

                    JSONArray candidatos = lista.getJSONArray("candidatos");
                    for(int k = 0; k < candidatos.length(); k++) {
                        JSONObject candidato = candidatos.getJSONObject(k);
                        Candidato entity_candidato = new Candidato();
                        entity_candidato.iden = candidato.getInt("id");
                        entity_candidato.nombreCompleto = candidato.getString("nombre_completo");
                        if(candidato.isNull("foto")) {
                            entity_candidato.fotoURL = null;
                        } else {
                            entity_candidato.fotoURL = candidato.getJSONObject("foto").getString("path");
                        }
                        entity_candidato.posicion = candidato.getInt("posicion");
                        entity_candidato.seccionLista = candidato.getString("seccion_lista");

                        entity_candidato.lista = entity_lista;

                        //Log.i(ConstantClass.TAG, "Candidato: " + entity_candidato.toString());
                        entity_candidato.save();
                    }
                }

            } else if(votacion.getString("class_name").equals(ConstantClass.VOTACION_REFERENDUM)) {
                VotacionReferendum entity_votacion = new VotacionReferendum();
                entity_votacion.iden = votacion.getInt("id");
                entity_votacion.nombre = votacion.getString("nombre");
                entity_votacion.descripcion = votacion.getString("descripcion");
                if(votacion.isNull("logotipo")) {
                    entity_votacion.logotipoURL = null;
                } else {
                    entity_votacion.logotipoURL = votacion.getJSONObject("logotipo").getString("path");
                }
                entity_votacion.votoEnBlanco = votacion.getBoolean("voto_en_blanco");
                entity_votacion.eventoElectoral = evento;

                entity_votacion.save();

                JSONArray preguntas = votacion.getJSONArray("preguntas");
                for(int j = 0; j < preguntas.length(); j++) {
                    JSONObject lista = preguntas.getJSONObject(j);
                    Pregunta entity_pregunta = new Pregunta();
                    entity_pregunta.iden = lista.getInt("id");
                    entity_pregunta.pregunta = lista.getString("pregunta");
                    entity_pregunta.orden = lista.getInt("orden");
                    entity_pregunta.multirespuesta = lista.getBoolean("multirespuesta");
                    entity_pregunta.votacionReferendum = entity_votacion;
                    entity_pregunta.save();

                    JSONArray respuestas = lista.getJSONArray("respuestas");
                    for(int k = 0; k < respuestas.length(); k++) {
                        JSONObject respuesta = respuestas.getJSONObject(k);
                        Respuesta entity_respuesta = new Respuesta();
                        entity_respuesta.iden = respuesta.getInt("id");
                        entity_respuesta.respuesta = respuesta.getString("respuesta");
                        entity_respuesta.orden = respuesta.getInt("orden");
                        entity_respuesta.pregunta = entity_pregunta;

                        entity_respuesta.save();
                    }
                }


            }
        }

        Intent intent = new Intent();
        intent.setAction(ConstantClass.ACTION_EVENTO_GUARDADO);
        intent.putExtra("idem", evento.iden);
        sendBroadcast(intent);

        Log.i(ConstantClass.TAG, "Object: " + evento.toString());
    }

    private String getEvento(String token) throws IOException {

        String url = String.format(ConstantClass.API_GET_EVENTO, dni, token);

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.i(ConstantClass.TAG, "Error descargando el evento.");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    private String getConvocatorias() throws IOException {

        String url = String.format(ConstantClass.API_GET_CONVOCATORIAS, dni);
        Log.i(ConstantClass.TAG, url);
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.i(ConstantClass.TAG, "Error consultado las convocatorias.");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

}
