package me.rayco.referendumelectronico;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;


public class EventosActivity extends ActionBarActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private BroadcastReceiver mReceiver;
    private TextView textEmpty;
    List<EventoElectoral> eventos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos);

        Log.i(ConstantClass.TAG, "En eventosActivity");

        checkPlayServices();

        // Comprobamos la validez del usuario (dni y telefono)
        final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
        Seguridad seguridad = new Seguridad(this.getApplicationContext());
        seguridad.esUsuarioValido(dni);

        mRecyclerView = (RecyclerView) findViewById(R.id.eventos_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        this.eventos = EventoElectoral.getAll();
        mAdapter = new EventosAdapter(this.eventos);
        mRecyclerView.setAdapter(mAdapter);

        textEmpty = (TextView) findViewById(R.id.eventos_texto_empty);
        if(this.eventos.size() > 0) {
            textEmpty.setText("");
            textEmpty.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        Log.i(ConstantClass.TAG, "No se puede volver, si se esta en eventos activity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_eventos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        if (id == R.id.action_refrescar) {
            Log.i(ConstantClass.TAG, "Pulsado en boton REFRESCAR EVENTOS");
            Intent intent = new Intent(getApplicationContext(), EventoElectoralService.class);
            intent.setAction(ConstantClass.ACTION_GET_CONVOCATORIAS);
            getApplicationContext().startService(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        ConstantClass.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(ConstantClass.TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = new Intent(getApplicationContext(), EventoElectoralService.class);
        intent.setAction(ConstantClass.ACTION_GET_CONVOCATORIAS);
        getApplicationContext().startService(intent);

        IntentFilter intentFilter = new IntentFilter(ConstantClass.ACTION_EVENTO_GUARDADO);

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                Integer idem = intent.getIntExtra("idem", -1);

                eventos = EventoElectoral.getAll();
                mAdapter = new EventosAdapter(eventos);
                mRecyclerView.setAdapter(mAdapter);

                if(eventos.size() > 0) {
                    textEmpty.setText("");
                    textEmpty.setVisibility(View.INVISIBLE);
                }

            }
        };

        if(eventos.size() > 0) {
            textEmpty.setText("");
            textEmpty.setVisibility(View.INVISIBLE);
        }

        this.registerReceiver(mReceiver, intentFilter);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(ConstantClass.NOTIFICACION_EVENTO_CONVOCADO);
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.unregisterReceiver(this.mReceiver);
    }
}
