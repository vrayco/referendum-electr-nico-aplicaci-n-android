package me.rayco.referendumelectronico;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;

public class EventosAdapter extends RecyclerView.Adapter<EventosAdapter.ViewHolder> {
    private List<EventoElectoral> eventos;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public EventoElectoral evento;
        public ImageView icon;
        public TextView firstLine;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            firstLine = (TextView) v.findViewById(R.id.firstLine);
            icon = (ImageView) v.findViewById(R.id.iconEstado);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "onClick " + getPosition() + " nombre: " + evento.nombre);
            Intent intent = new Intent(v.getContext(), EventoActivity.class);
            intent.putExtra("id_evento", evento.iden);
            v.getContext().startActivity(intent);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public EventosAdapter(List<EventoElectoral> _eventos) {
        eventos = _eventos;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EventosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.evento_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        EventoElectoral ev = eventos.get(position);
        holder.evento = ev;
        holder.firstLine.setText(ev.nombre);

        if(ev.logotipoURL != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + ev.logotipoURL, holder.icon);
        }

//        String url = "http://www.cafeinaexpress.com/wp-content/uploads/2016/04/buenavista_1.jpg";
//        imageLoader.displayImage(url, holder.icon);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return eventos.size();
    }
}