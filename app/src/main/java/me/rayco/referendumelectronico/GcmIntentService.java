/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.rayco.referendumelectronico;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.io.*;
import java.security.*;
import java.security.spec.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import me.rayco.referendumelectronico.model.EventoElectoral;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                String data_str = intent.getStringExtra("data");
                Log.i(ConstantClass.TAG, "Mensaje GCM recibido: " + data_str);
                if(data_str != null) {
                    try {
                        JSONObject data = new JSONObject(data_str);

                        if (!data.isNull("registro")) {
                            JSONObject registro = new JSONObject(data.getString("registro"));
                            // Dos opciones: O es codigo1 o es el mensaje de fin del proceso
                            if (!registro.isNull("codigo_1")) {
                                // Llamo al servicio, para que devuelva el código al servidor y existe esta iniciada la solicitud en prefs
                                Log.i(ConstantClass.TAG, "Detectado CODIGO_1");
                                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                                if (prefs.getBoolean(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_1, false)) {
                                    Intent mServiceIntent = new Intent(this, RegistroService.class);
                                    mServiceIntent.setAction(ConstantClass.ACTION_DEVOLVER_CODIGO1);
                                    mServiceIntent.putExtra(ConstantClass.EXTRA_CODIGO1, registro.getString("codigo_1"));

                                    this.startService(mServiceIntent);

                                } else {
                                    Log.i(ConstantClass.TAG, "Ignorado CODIGO_1, no hay solicitud abierta");
                                }

                            } else if (!registro.isNull("estado")) {

                            }
                        } else if (!data.isNull("cifrado")) {
                            Log.i(ConstantClass.TAG, "mensaje cifrado.");
                            JSONObject cifrado = new JSONObject(data.getString("cifrado"));
                            String mensaje_cifrado = cifrado.getString("mensaje");
                            Log.i(ConstantClass.TAG, "mensaje cifrado: " + mensaje_cifrado);
                            //***********************/
                            KeyStore ks = null;
                            try {
                                ks = KeyStore.getInstance(ConstantClass.KEY_STORE);
                                ks.load(null);
                                KeyStore.Entry entry = ks.getEntry(ConstantClass.APP_KEY_RSA, null);
                                if (entry != null) {
                                    if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                                        Log.i(ConstantClass.TAG, "Not an instance of a PrivateKeyEntry");
                                    }
                                    PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
                                    Log.i(ConstantClass.TAG, "Clave privada: " + privateKey.toString());

                                    Rsa rsa = new Rsa();
                                    String mensaje = rsa.privateDecrypt(mensaje_cifrado, privateKey);
                                    Log.i(ConstantClass.TAG, "Mensaje descifrado: " + mensaje);

                                }
                            } catch (Exception e) {
                                Log.i(ConstantClass.TAG, e.getMessage());
                            }
                            //************************/


                        } else if (!data.isNull("cifrado2")) {
                            Log.i(ConstantClass.TAG, "Mensaje cifrado con key privada del SERVER. Intentado descifrar");
                            JSONObject cifrado = new JSONObject(data.getString("cifrado2"));
                            String str_publicKey = cifrado.getString("pubKey");
                            String mensaje_cifrado = cifrado.getString("mensaje");
                            Log.i(ConstantClass.TAG, "Mensaje: " + mensaje_cifrado);

                            try {

                                Rsa rsa = new Rsa();
                                PublicKey publicKey = rsa.getPublicKey(str_publicKey);
                                Log.i(ConstantClass.TAG, publicKey.toString());

                                String mensaje_descifrado = rsa.publicDecrypt(mensaje_cifrado, publicKey);
                                Log.i(ConstantClass.TAG, "mensaje descifrado: " + mensaje_descifrado);
                            } catch (UnsupportedEncodingException e) {
                                Log.i(ConstantClass.TAG, "aqui1");
                                Log.i(ConstantClass.TAG, e.getMessage());
                            } catch (NoSuchAlgorithmException e) {
                                Log.i(ConstantClass.TAG, "aqui2");
                                Log.i(ConstantClass.TAG, e.getMessage());
                            } catch (InvalidKeySpecException e) {
                                Log.i(ConstantClass.TAG, "aqui3");
                                Log.i(ConstantClass.TAG, e.getMessage());
                            }
                            Log.i(ConstantClass.TAG, "aqui4");

                        } else if (!data.isNull("compartir_clave_publica")) {
                            Log.i(ConstantClass.TAG, "Recibido código cifrado para validar clave pública entregada");
                            JSONObject datos = new JSONObject(data.getString("compartir_clave_publica"));
                            String codigo_cifrado = datos.getString("codigo_cifrado");

                            Log.i(ConstantClass.TAG, "Codigo cifrado: " + codigo_cifrado);
                            for(Integer i = 0; i <= 30; i++) {
                                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                                if (prefs.getBoolean(ConstantClass.PROPERTY_ENTREGA_CLAVE_PUBLICA_ABIERTO, false)) {
                                    Intent mServiceIntent = new Intent(this, RegistroService.class);
                                    mServiceIntent.setAction(ConstantClass.ACTION_CONFIRMAR_CLAVE_PUBLICA);
                                    mServiceIntent.putExtra(ConstantClass.EXTRA_CODIGO_CIFRADO, codigo_cifrado);
                                    this.startService(mServiceIntent);
                                    break;
                                } else {
                                    Log.i(ConstantClass.TAG, "Ignorado CODIGO, el proceso de entrega de la clave pública está cerrado");
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        } else if(!data.isNull("validar_voto")) {
                            Log.i(ConstantClass.TAG, "Recibido código cifrado para validar voto." );
                            JSONObject datos = new JSONObject(data.getString("validar_voto"));
                            Log.i(ConstantClass.TAG, "Mensaje GCM de validar voto: " + datos.toString() );
                            String codigo = datos.getString("codigo_validacion");
                            String l1 = datos.getString("l1");

                            final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                            final Integer eventoId = prefs.getInt(ConstantClass.PROPERTY_VOTO_ENVIADO, -1);
                            if (eventoId != -1) {
                                Intent mServiceIntent = new Intent(this, VotoService.class);
                                mServiceIntent.setAction(ConstantClass.ACTION_VALIDAR_VOTO);
                                mServiceIntent.putExtra(ConstantClass.EXTRA_CODIGO_VALIDACION, codigo);
                                mServiceIntent.putExtra(ConstantClass.EXTRA_L1, l1);
                                mServiceIntent.putExtra(ConstantClass.EXTRA_EVENTO_ID, eventoId);
                                this.startService(mServiceIntent);
                            } else {
                                Log.i(ConstantClass.TAG, "Ignorado CODIGO PARA VALIDAR VOTO");
                            }
                        } else if(!data.isNull("convocatoria_evento_electoral")) {
                            JSONObject datos = new JSONObject(data.getString("convocatoria_evento_electoral"));
                            Integer id = datos.getInt("id");
                            EventoElectoral evento = EventoElectoral.get(id);
                            if(evento == null) {
                                Log.i(ConstantClass.TAG, "RECIBIDO CONVOCATORIA DE EVENTO ELECTORAL. DESCARGAMOS");
                                Intent intent2 = new Intent(getApplicationContext(), EventoElectoralService.class);
                                intent2.setAction(ConstantClass.ACTION_GET_CONVOCATORIAS);
                                getApplicationContext().startService(intent2);

                                Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notifications);

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(this)
                                                .setSmallIcon(R.drawable.votacion)
                                                .setContentTitle("EVENTO ELECTORAL CONVOCADO")
                                                .setContentText("Estas convocado a participar en un nuevo evento electoral")
                                                .setSound(alarmSound)
                                                .setLights(Color.BLUE, 3000, 3000)
                                                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

                                Intent resultIntent = new Intent(this, EventosActivity.class);

                                PendingIntent resultPendingIntent =
                                        PendingIntent.getActivity(
                                                this,
                                                0,
                                                resultIntent,
                                                PendingIntent.FLAG_UPDATE_CURRENT
                                        );

                                mBuilder.setContentIntent(resultPendingIntent);

                                // Sets an ID for the notification
                                int mNotificationId = ConstantClass.NOTIFICACION_EVENTO_CONVOCADO;
                                // Gets an instance of the NotificationManager service
                                NotificationManager mNotifyMgr =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                // Builds the notification and issues it.
                                mNotifyMgr.notify(mNotificationId, mBuilder.build());
                            }
                        } else if(!data.isNull("apertura_evento_electoral")) {
                            JSONObject datos = new JSONObject(data.getString("apertura_evento_electoral"));
                            Integer id = datos.getInt("id");
                            EventoElectoral evento = EventoElectoral.get(id);
                            if(evento == null) {
                                Log.i(ConstantClass.TAG, "RECIBIDO APERTURA DE EVENTO ELECTORAL");
                                Intent intent2 = new Intent(getApplicationContext(), EventoElectoralService.class);
                                intent2.setAction(ConstantClass.ACTION_GET_CONVOCATORIAS);
                                getApplicationContext().startService(intent2);

                            } else {
                                evento.estado = "ABIERTO";
                                evento.save();
                            }

                            Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notifications);

                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(this)
                                            .setSmallIcon(R.drawable.votacion)
                                            .setContentTitle("EVENTO ELECTORAL ABIERTO")
                                            .setContentText("Ya puede enviar su voto")
                                            .setSound(alarmSound)
                                            .setLights(Color.BLUE, 3000, 3000)
                                            .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

                            Intent resultIntent = new Intent(this, EventoActivity.class);
                            resultIntent.putExtra("id_evento", evento.iden);

                            PendingIntent resultPendingIntent =
                                    PendingIntent.getActivity(
                                            this,
                                            0,
                                            resultIntent,
                                            PendingIntent.FLAG_UPDATE_CURRENT
                                    );

                            mBuilder.setContentIntent(resultPendingIntent);

                            // Sets an ID for the notification
                            int mNotificationId = ConstantClass.NOTIFICACION_EVENTO_ABIERTO;
                            // Gets an instance of the NotificationManager service
                            NotificationManager mNotifyMgr =
                                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            // Builds the notification and issues it.
                            mNotifyMgr.notify(mNotificationId, mBuilder.build());

                        } else if(!data.isNull("cierre_evento_electoral")) {
                            JSONObject datos = new JSONObject(data.getString("cierre_evento_electoral"));
                            Integer id = datos.getInt("id");
                            EventoElectoral evento = EventoElectoral.get(id);
                            if(evento != null) {
                                evento.estado  = "CERRADO";
                                evento.save();
                            }
                        } else if(!data.isNull("escrutinio_evento_electoral")) {

                            JSONObject datos = new JSONObject(data.getString("escrutinio_evento_electoral"));
                            Integer id = datos.getInt("id");
                            EventoElectoral evento = EventoElectoral.get(id);
                            if(evento != null) {

                                evento.estado  = "FINALIZADO";
                                evento.save();

                                Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notifications);

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(this)
                                                .setSmallIcon(R.drawable.votacion)
                                                .setContentTitle("EVENTO ELECTORAL FINALIZADO")
                                                .setContentText("Ha terminado el escrutinio")
                                                .setSound(alarmSound)
                                                .setLights(Color.BLUE, 3000, 3000)
                                                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

                                Intent resultIntent = new Intent(this, EventoActivity.class);
                                resultIntent.putExtra("id_evento", evento.iden);

                                PendingIntent resultPendingIntent =
                                        PendingIntent.getActivity(
                                                this,
                                                0,
                                                resultIntent,
                                                PendingIntent.FLAG_UPDATE_CURRENT
                                        );

                                mBuilder.setContentIntent(resultPendingIntent);

                                // Sets an ID for the notification
                                int mNotificationId = ConstantClass.NOTIFICACION_EVENTO_FINALIZADO;
                                // Gets an instance of the NotificationManager service
                                NotificationManager mNotifyMgr =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                // Builds the notification and issues it.
                                mNotifyMgr.notify(mNotificationId, mBuilder.build());
                            }
                        } else {
                            Log.i(ConstantClass.TAG, "Mensaje GCM recibido pero no reconocido: " + extras.toString());
                        }


                    } catch (JSONException e) {
                        Log.i(ConstantClass.TAG, "Json exception");
                        e.printStackTrace();
                    }
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_stat_gcm)
                        .setContentTitle("GCM Notification")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
