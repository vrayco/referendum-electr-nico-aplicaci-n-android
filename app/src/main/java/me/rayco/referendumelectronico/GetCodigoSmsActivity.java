package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;


public class GetCodigoSmsActivity extends ActionBarActivity {

    String codigo2 = null;
    Context context = null;
    EditText editText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_codigo_sms);

        Intent intent = getIntent();

        // Si he recibido el codigo2 vía sms, ejecuto la tarea
        codigo2 = intent.getStringExtra(ConstantClass.EXTRA_CODIGO2);
        if(codigo2 != null) {
            final TextView textView = (TextView) findViewById(R.id.textViewErrorSms);
            textView.setVisibility(View.VISIBLE);
            textView.setText(codigo2);
            context = getApplicationContext();

            new EnviarCodigoSmsTask().execute();

            // Paso a esperar por la tarea en la activity Wait
            Intent intent2 = new Intent(context, WaitActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent2);
        }

        // Si se intento validar codigo2 y fallo, muestro error
        String error = intent.getStringExtra(ConstantClass.EXTRA_MSG);
        if(error != null) {
            final TextView textView = (TextView) findViewById(R.id.textViewErrorSms);
            textView.setVisibility(View.VISIBLE);
            textView.setText(error);
        }

        editText = (EditText) findViewById(R.id.editTextSms);
        editText.getBackground().setColorFilter(Color.parseColor("#415ba0"), PorterDuff.Mode.SRC_ATOP);
    }

    public void EnviarCodigo(View v) {

        final Button button = (Button) findViewById(R.id.buttonRegistrarSms);

        // Desactivo botón
        button.setEnabled(false);
        // Desactivo editText
        final EditText editText = (EditText) findViewById(R.id.editTextSms);
        editText.setEnabled(false);
        // Muestro barra de progreso
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarSms);
        progressBar.setVisibility(View.VISIBLE);

        codigo2 = editText.getText().toString();
        context = getApplicationContext();

        // Llamo al servicio para registrar la app en el servidor
        new EnviarCodigoSmsTask().execute();

        // Paso a esperar por la tarea en la activity Wait
        Intent intent = new Intent(context, WaitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private class EnviarCodigoSmsTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                Registro registro = new Registro(getApplicationContext());
                if (registro.sendCodigo2ToBackend(codigo2))
                    return "Executed";

            } catch (IOException e) {
                e.printStackTrace();
            }

            return "Fail";
        }

        @Override
        protected void onPostExecute(String result) {
            // Trabajo hecho. Vuelvo a StartActivity
            Intent intent;
            if(result.equals("Executed")) {
                Log.i(ConstantClass.TAG, "Enhorabuena! La app ha terminado el proceso de validación");

                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_2);
                editor.putBoolean(ConstantClass.PROPERTY_VALIDACION_COMPLETADA, true);
                editor.commit();

                intent = new Intent(context, StartActivity.class);
            } else {
                intent = new Intent(context, GetCodigoSmsActivity.class);
                // Añadir mensaje de dni erroneo.
                intent.putExtra(ConstantClass.EXTRA_MSG, ConstantClass.MSG_ERROR_SMS);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_get_codigo_sms, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onBackPressed() {
        Log.i(ConstantClass.TAG, "No se puede volver, si se esta en espera del codigo sms");
    }
}
