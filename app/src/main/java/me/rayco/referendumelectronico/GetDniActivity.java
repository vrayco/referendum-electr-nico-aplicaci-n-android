package me.rayco.referendumelectronico;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


public class GetDniActivity extends ActionBarActivity {

    String dni = null;
    Context context = null;
    EditText editText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_dni);

        Intent intent = getIntent();
        String error = intent.getStringExtra(ConstantClass.EXTRA_MSG);
        if(error != null) {
            final TextView textView = (TextView) findViewById(R.id.textViewErrorDni);
            textView.setVisibility(View.VISIBLE);
            textView.setText(error);
        }

        // Si ha habido un error, recargo el editText con el valor del usuario
        final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        dni = prefs.getString(ConstantClass.PROPERTY_VALUE_EDITTEXTDNI, "");

        editText = (EditText) findViewById(R.id.editTextDni);
        if(!dni.isEmpty()) {
            editText.setText(dni);
        }

        editText.getBackground().setColorFilter(Color.parseColor("#415ba0"), PorterDuff.Mode.SRC_ATOP);

    }

    public void RegistrarApp(View v) {

        final Button button = (Button) findViewById(R.id.buttonRegistrarDni);

        // Desactivo botón
        button.setEnabled(false);
        // Desactivo editText
        final EditText editText = (EditText) findViewById(R.id.editTextDni);
        editText.setEnabled(false);
        // Muestro barra de progreso
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarDni);
        progressBar.setVisibility(View.VISIBLE);

        dni = editText.getText().toString();
        context = getApplicationContext();

        // Llamo al servicio para registrar la app en el servidor
        new IniciarRegistroTask().execute();

        // Almaceno temporalmente el valor introducido, por si hay un error recargar el editText
        final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ConstantClass.PROPERTY_VALUE_EDITTEXTDNI, dni);
        editor.commit();

        // Paso a esperar por la tarea en la activity Wait
        Intent intent = new Intent(context, WaitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private class IniciarRegistroTask extends AsyncTask<String, Void, String> {

        Integer code = null;
        String message = null;

        @Override
        protected String doInBackground(String... params) {
            Log.i(ConstantClass.TAG, "Inicio la solicitud de registro de la aplicación con el dni: " + dni);

            // Abro la solicitud de registro, para que cuando se reciba el codigo1 se sepa que no es un ataque.
            final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_1, true);
            editor.commit();

            Registro registro = new Registro(getApplicationContext());
            JSONObject resultado = registro.IniciarRegistro(dni);

            try {
                code = resultado.getInt("codigo");
                if(code == 200)
                    return "Executed";
                else {
                    JSONObject contendido = resultado.getJSONObject("contenido");
                    message = contendido.getString("message");
                    return "Fail";
                }

            } catch (JSONException e) {
                return "Fail";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            // Trabajo hecho. Vuelvo a StartActivity
            Intent intent;
            if(result.equals("Executed")) {
                Log.i(ConstantClass.TAG, "Se ha iniciado correctamente la solicitud de registro");
                // Elimino el valor temporal del editText almacenado, al pulsar el botón
                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(ConstantClass.PROPERTY_VALUE_EDITTEXTDNI);
                editor.commit();
                intent = new Intent(context, StartActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                Log.i(ConstantClass.TAG, "Problemas iniciando la solicitud");
                final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_1);
                editor.commit();

                // Vuelvo a solicitar dni y añado mensaje de mensaje de errr.
                intent = new Intent(context, GetDniActivity.class);
                intent.putExtra(ConstantClass.EXTRA_MSG, message + " [ERROR " + code + "]");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    @Override
    public void onBackPressed() {
        Log.i(ConstantClass.TAG, "No se puede volver, si se esta en espera del dni");
    }
}
