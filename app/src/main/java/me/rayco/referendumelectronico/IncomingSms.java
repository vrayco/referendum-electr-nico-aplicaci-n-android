package me.rayco.referendumelectronico;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rayco on 3/4/15.
 */
public class IncomingSms extends BroadcastReceiver {
    final SmsManager sms = SmsManager.getDefault();

    @Override
    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
//                  String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Pattern p = Pattern.compile(ConstantClass.REGEX_TEXT_SMS_CODIGO2);
                    Matcher m = p.matcher(message);

                    if(m.find()) {
                        Log.i(ConstantClass.TAG, "Detectado CODIGO_2 en SMS");
                        final SharedPreferences prefs = context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                        if(prefs.getBoolean(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_2, false)) {
                            Intent intent2 = new Intent(context, GetCodigoSmsActivity.class);
                            intent2.putExtra(ConstantClass.EXTRA_CODIGO2, m.group(2));
                            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent2);
                        } else {
                            Log.i(ConstantClass.TAG, "Ignorado CODIGO_2, no hay solicitud en fase 2");
                        }
                    }
                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }


}
