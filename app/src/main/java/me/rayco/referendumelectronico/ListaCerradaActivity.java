package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.Lista;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;


public class ListaCerradaActivity extends ActionBarActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Context context = null;
    Lista lista = null;

    ImageView logotipo = null;
    TextView nombreAbreviado = null;
    TextView nombreCompleto = null;
    Button boton = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cerrada);

        Log.i(ConstantClass.TAG, "En Listas Cerradas Activity");
        this.context = this.getApplicationContext();

        Intent intent = getIntent();
        Integer idLista = intent.getIntExtra("id_lista", -1);
        this.lista = Lista.get(idLista);

        // Logotipo
        this.logotipo = (ImageView) findViewById(R.id.lista_cerrada_logotipo);
        if(this.lista.path_logotipo != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + this.lista.path_logotipo, this.logotipo);
        }

        // Nombre Abreviado
        this.nombreAbreviado = (TextView) findViewById(R.id.lista_cerrada_nombre_abrevidado);
        this.nombreAbreviado.setText(this.lista.nombreAbreviado);

        // Nombre Completo
        this.nombreCompleto = (TextView) findViewById(R.id.lista_cerrada_nombre_completo);
        this.nombreCompleto.setText(this.lista.nombreCompleto);

        mRecyclerView = (RecyclerView) findViewById(R.id.lista_cerrada_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<Candidato> candidatos = this.lista.candidatosOrdenados();

        mAdapter = new CandidatosAdapter(candidatos);
        mRecyclerView.setAdapter(mAdapter);

        Log.i(ConstantClass.TAG, this.lista.toString());
        // Si se ha escogido esta opción, cambio el texto del botón
        this.boton = (Button) findViewById(R.id.lista_cerrada_seleccionar);
        if(this.lista.seleccionado)
            this.boton.setText(R.string.deseleccionar_opcion);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setTitle(lista.getNombreEvento());
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setSubtitle(this.lista.getNombreVotacion());
        } else {
            setTitle(lista.getNombreEvento() + " - " + this.lista.getNombreVotacion());
        }
    }

    public void Seleccionar(View view) {

        if(this.lista.seleccionado) {
            this.lista.seleccionado = false;
            this.lista.save();
            Toast.makeText(this, "¡Ok! Has deseleccionado a " + this.lista.nombreAbreviado,
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context, ListasActivity.class);
            intent.putExtra("id_votacion", this.lista.votacionListasCerradas.iden);
            intent.putExtra("tipo_votacion", "VotacionListasCerradas");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            view.getContext().startActivity(intent);
        } else {
            // Comprabamos que el número de listas cerradas seleccionadas no excede de las permitidas
            Integer numMaxVotos = this.lista.votacionListasCerradas.votosPorElector;
            Integer votos = 0;
            List<Lista> listas = this.lista.votacionListasCerradas.Listas();
            for(final Lista l : listas)
                if(l.seleccionado)
                    votos++;

            if(votos >= numMaxVotos) {
                Toast.makeText(this,"No puedes seleccionar más opciones",
                        Toast.LENGTH_SHORT).show();
            } else {
                this.lista.seleccionado = true;
                this.lista.save();
                Toast.makeText(this, "¡Ok! Has seleccionado a " + this.lista.nombreAbreviado,
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ListasActivity.class);
                intent.putExtra("id_votacion", this.lista.votacionListasCerradas.iden);
                intent.putExtra("tipo_votacion", "VotacionListasCerradas");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                view.getContext().startActivity(intent);
            }
        }
    }


}
