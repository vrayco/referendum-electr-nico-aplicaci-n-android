package me.rayco.referendumelectronico;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.Lista;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;


public class ListasActivity extends ActionBarActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Context context = null;
    String tipoLista = null;
    int idVotacion = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listas);

        Log.i(ConstantClass.TAG, "En Listas Activity");
        this.context = this.getApplicationContext();

        mRecyclerView = (RecyclerView) findViewById(R.id.listas_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        this.idVotacion = intent.getIntExtra("id_votacion", -1);
        this.tipoLista = intent.getStringExtra("tipo_votacion");

        Log.i(ConstantClass.TAG, this.idVotacion + " " + this.tipoLista);

        if(this.tipoLista.equals("VotacionListasCerradas")) {
            Log.i(ConstantClass.TAG, "id_votacion= " + this.idVotacion);
            VotacionListasCerradas votacion = VotacionListasCerradas.get(idVotacion);
            if(votacion == null)
                Log.i(ConstantClass.TAG, "Votacion=null");
            Log.i(ConstantClass.TAG, "Votacion=" + votacion.toString());
            this.votacionListasCerradas(votacion);
        }
        else {
            VotacionListasAbiertas votacion = VotacionListasAbiertas.get(idVotacion);
            this.votacionListasAbiertas(votacion);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(this.tipoLista.equals("VotacionListasCerradas")) {
            VotacionListasCerradas votacion = VotacionListasCerradas.get(idVotacion);
            this.votacionListasCerradas(votacion);
        } else if(this.tipoLista.equals("VotacionListasAbiertas")) {
            VotacionListasAbiertas votacion = VotacionListasAbiertas.get(idVotacion);
            this.votacionListasAbiertas(votacion);
        }

    }

    private void votacionListasCerradas(VotacionListasCerradas votacion) {
        Log.i(ConstantClass.TAG, "evento= " + votacion.toString());
        // Titulo del action bar
        setTitle(votacion.eventoElectoral.nombre);
        // Subtitulo nombre votación
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setSubtitle(votacion.nombre);
        }

        List<Lista> listas = votacion.Listas();
        mAdapter = new ListasAdapter(this, listas);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void votacionListasAbiertas(VotacionListasAbiertas votacion) {
        // Titulo del action bar
        setTitle(votacion.eventoElectoral.nombre);
        // Subtitulo nombre votación
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setSubtitle(votacion.nombre);
        }

        List<Lista> listas = votacion.Listas();
        mAdapter = new ListasAdapter(this, listas);
        mRecyclerView.setAdapter(mAdapter);
    }


}
