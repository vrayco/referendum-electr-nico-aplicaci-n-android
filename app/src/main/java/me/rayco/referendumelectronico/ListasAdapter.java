package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import me.rayco.referendumelectronico.model.Lista;

public class ListasAdapter extends RecyclerView.Adapter<ListasAdapter.ViewHolder> {
    private Context context;
    private List<Lista> listas;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView nombreAbreviado;
        public TextView nombreCompleto;
        public ImageView logotipo;
        public ImageView iconSeleccionado;

        public Lista lista;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            nombreAbreviado = (TextView) v.findViewById(R.id.lista_nombre_abreviado);
            nombreCompleto = (TextView) v.findViewById(R.id.lista_nombre_completo);
            logotipo = (ImageView) v.findViewById(R.id.lista_logotipo);
            iconSeleccionado = (ImageView) v.findViewById(R.id.lista_icon_seleccionado);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "onClick en lista" + getPosition() + " lista: " + lista.toString());
            Intent intent = null;
            if(lista.votacionListasAbiertas != null) {
                intent = new Intent(v.getContext(), ListaAbiertaActivity.class);
            } else {
                intent = new Intent(v.getContext(), ListaCerradaActivity.class);
            }

            intent.putExtra("id_lista", lista.iden);
            v.getContext().startActivity(intent);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListasAdapter(Context _context, List<Lista> _listas) {
        this.context = _context;
        this.listas = _listas;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Lista lista = listas.get(position);
        holder.lista = lista;
        holder.nombreAbreviado.setText(lista.nombreAbreviado);
        holder.nombreCompleto.setText(lista.nombreCompleto);

        // Configuro ANDROID UNIVERSAL IMAGE LOADER
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this.context.getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        if(lista.path_logotipo != null)
            ImageLoader.getInstance().displayImage(ConstantClass.RESOURCES_URL + "/" + lista.path_logotipo, holder.logotipo);

        if(lista.votacionListasCerradas != null) {
            if (lista.seleccionado)
                holder.iconSeleccionado.setVisibility(View.VISIBLE);
        } else if(lista.votacionListasAbiertas != null)
            if(lista.getNumeroCandidatosSeleccionados(lista) > 0)
                holder.iconSeleccionado.setVisibility(View.VISIBLE);

        Log.i(ConstantClass.TAG, "Listas en LISTASADAPTER size = " + listas.size());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.listas.size();
    }
}