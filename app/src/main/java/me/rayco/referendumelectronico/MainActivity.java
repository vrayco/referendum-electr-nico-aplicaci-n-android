package me.rayco.referendumelectronico;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;

import me.rayco.referendumelectronico.model.EventoElectoral;


public class MainActivity extends ActionBarActivity {

    TextView mDisplay;
    Context context;

    PrivateKey privateKey = null;
    PublicKey  publicKey = null;
    String secreto = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPlayServices();

        // Comprobamos la validez del usuario (dni y telefono)
        final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
        Seguridad seguridad = new Seguridad(this.getApplicationContext());
        seguridad.esUsuarioValido(dni);

        Log.i(ConstantClass.TAG, "En MainActivity");

        final Button button1 = (Button) findViewById(R.id.send);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Log.i(ConstantClass.TAG, "Pulsado en boton send");
                Intent intent = new Intent(getApplicationContext(), EventoElectoralService.class);
                intent.setAction(ConstantClass.ACTION_GET_CONVOCATORIAS);
                getApplicationContext().startService(intent);
            }
        });

        final Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context context = v.getContext().getApplicationContext();
                // Para desarrollo, fuerzo a que registre en cada ejecución
                final SharedPreferences prefs = v.getContext().getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(ConstantClass.PROPERTY_REG_ID);
                editor.remove(ConstantClass.PROPERTY_VALIDACION_COMPLETADA);
                editor.commit();
                Log.i(ConstantClass.TAG, "reiniciado");
            }
        });

        final Button button3 = (Button) findViewById(R.id.send2);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                String mensaje = "Hola holita, cifrado con key publica del server!!!";
                String publicKeyServer =   "-----BEGIN PUBLIC KEY-----\n"+
                        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDYPkHLGUcEHrZQqBZdqvIA6PEn\n" +
                        "3GHksmMVTNPTUmKQqQVT3qk84lKvkE5wYgbh0fgA0rNiRmXtS2tbbpkFktlPQ1bT\n"+
                        "uIHmr0ZObfN3rZ2HeK4FexFm+r1QniIZuwbTfHkGS9OAYGfS64JWt2kzYBNqryYQ\n"+
                        "f3Ra/HmsD58edghdCwIDAQAB\n"+
                        "-----END PUBLIC KEY-----\n";

                Rsa rsa = new Rsa();

                try {
                    PublicKey publicKey = rsa.getPublicKey(publicKeyServer);
                    String mensaje_cifrado = rsa.publicEncrypt(mensaje, publicKey);

                    Log.i(ConstantClass.TAG, mensaje_cifrado);
                    JSONObject data = new JSONObject();
                    try {
                        data.put("mensaje_cifrado", mensaje_cifrado);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.i(ConstantClass.TAG, "Boton3 pulsado, intento enviar: "+ data.toString());
                    Log.i(ConstantClass.TAG, "URL: " + ConstantClass.LAB_ENVIAR_MENSAJE_CIFRADO2);
                    Intent intent = new Intent(getApplicationContext(), ComunicacionBackendService.class);
                    intent.setAction(ConstantClass.LAB_ACTION_SEND);
                    intent.putExtra(ConstantClass.EXTRA_JSON_DATA, data.toString());
                    intent.putExtra(ConstantClass.EXTRA_URL, ConstantClass.LAB_ENVIAR_MENSAJE_CIFRADO2);
                    getApplicationContext().startService(intent);

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }

            }
        });

//        KeyStore ks = null;
//        try {
//            ks = KeyStore.getInstance(ConstantClass.KEY_STORE);
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            ks.load(null);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (CertificateException e) {
//            e.printStackTrace();
//        }
//
//        KeyStore.Entry entry = null;
//        try {
//            entry = ks.getEntry(ConstantClass.APP_KEY_RSA, null);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (UnrecoverableEntryException e) {
//            e.printStackTrace();
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//        }
//        if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
//            Log.w("E", "Not an instance of a PrivateKeyEntry");
//        }
//        else{
//            Log.w("E", "Got Key!");
//            privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
//            Log.i(ConstantClass.TAG, privateKey.toString());
//
//            publicKey = ((KeyStore.PrivateKeyEntry) entry).getCertificate().getPublicKey();
//            Log.i(ConstantClass.TAG, publicKey.toString());
//
//            Rsa rsa = new Rsa();
//
//            secreto = rsa.privateEncrypt("Hola mundo!!! Como estan ustedes??", privateKey);
//            Log.i(ConstantClass.TAG, "Secreto: " + secreto);
//
//            String mensaje = rsa.publicDecrypt(secreto, publicKey);
//            Log.i(ConstantClass.TAG, "Mensaje: " + mensaje);
//
//        }

//        Intent intent2 = new Intent(this, EventosActivity.class);
//        startActivity(intent2);

    }


    @Override
    public void onBackPressed() {
        Log.i(ConstantClass.TAG, "No se puede volver, si se esta en MainActivity");
    }

    protected void onResume() {
        super.onResume();
        // Check device for Play Services APK.
        checkPlayServices();
        // Comprobamos la validez del usuario (dni y telefono)
        final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
        Seguridad seguridad = new Seguridad(this.getApplicationContext());
        seguridad.esUsuarioValido(dni);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        ConstantClass.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(ConstantClass.TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}