package me.rayco.referendumelectronico;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.Pregunta;
import me.rayco.referendumelectronico.model.Respuesta;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;


public class PreguntaActivity extends ActionBarActivity {

    Pregunta pregunta = null;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private BroadcastReceiver mReceiver;

    TextView fraccion   = null;
    TextView enunciado  = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregunta);

        Intent intent = getIntent();
        Integer id_pregunta = intent.getIntExtra("id_pregunta", -1);
        String strFraccion = intent.getStringExtra("fraccion");
        Log.i(ConstantClass.TAG, "En pregunta activity, pregunta id: " + id_pregunta);
        if(id_pregunta != -1) {
            this.pregunta = Pregunta.get(id_pregunta);

            this.fraccion = (TextView) findViewById(R.id.pregunta_fraccion);
            this.enunciado = (TextView) findViewById(R.id.pregunta_enunciado);

            this.fraccion.setText(strFraccion);
            this.enunciado.setText(pregunta.pregunta);

            mRecyclerView = (RecyclerView) findViewById(R.id.respuestas_recycler_view);
            mRecyclerView.setHasFixedSize(true);

            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);

            List<Respuesta> respuestas = this.pregunta.respuestas();

            mAdapter = new RespuestasAdapter(respuestas);
            mRecyclerView.setAdapter(mAdapter);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                setTitle(R.string.app_name);
                android.support.v7.app.ActionBar ab = getSupportActionBar();
                ab.setSubtitle(pregunta.votacionReferendum.nombre);
            } else {
                setTitle(pregunta.votacionReferendum.nombre);
            }
        }

    }



    public void IniciarVotacion(View view) {
//        Intent intent3 = new Intent(this, VotacionesActivity.class);
//        intent3.putExtra("id_evento", this.id_evento);
//        this.startActivity(intent3);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(ConstantClass.ACTION_REFERENDUM_RESPUESTA);

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Integer iden = intent.getIntExtra("id_pregunta", -1);
                if(iden == pregunta.iden) {
                    List<Respuesta> respuestas = pregunta.respuestas();
                    mAdapter = new RespuestasAdapter(respuestas);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }
        };

        this.registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.unregisterReceiver(this.mReceiver);
    }

}
