package me.rayco.referendumelectronico;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import me.rayco.referendumelectronico.model.Pregunta;
import me.rayco.referendumelectronico.model.VotacionReferendum;


public class PreguntasActivity extends ActionBarActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private VotacionReferendum referendum = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preguntas);

        mRecyclerView = (RecyclerView) findViewById(R.id.preguntas_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        Integer id_votacion = intent.getIntExtra("id_votacion", -1);
        if(id_votacion != -1) {
            this.referendum = VotacionReferendum.get(id_votacion);
            List<Pregunta> preguntas = this.referendum.Preguntas();
            mAdapter = new PreguntasAdapter(this, preguntas);
            mRecyclerView.setAdapter(mAdapter);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                setTitle(R.string.app_name);
                android.support.v7.app.ActionBar ab = getSupportActionBar();
                ab.setSubtitle(this.referendum.eventoElectoral.nombre);
            } else {
                setTitle(this.referendum.nombre);
            }
        }

    }

    public void IniciarVotacion(View view) {
//        Intent intent3 = new Intent(this, VotacionesActivity.class);
//        intent3.putExtra("id_evento", this.id_evento);
//        this.startActivity(intent3);
    }

}
