package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.rayco.referendumelectronico.model.Pregunta;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;

public class PreguntasAdapter extends RecyclerView.Adapter<PreguntasAdapter.ViewHolder> {
    private Context context;
    private List<Pregunta> preguntas;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Pregunta pregunta;

        public TextView fraccion;
        public TextView enunciado;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            fraccion = (TextView) v.findViewById(R.id.pregunta_view_fraccion);
            enunciado = (TextView) v.findViewById(R.id.pregunta_view_enunciado);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "Click en pregunta: " + pregunta.toString());
            Intent intent = new Intent(v.getContext(), PreguntaActivity.class);
            intent.putExtra("id_pregunta", this.pregunta.iden);
            intent.putExtra("fraccion", fraccion.getText());
            v.getContext().startActivity(intent);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PreguntasAdapter(Context _context, List<Pregunta> _preguntas) {
        this.context = _context;
        this.preguntas = _preguntas;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PreguntasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pregunta_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i(ConstantClass.TAG, "PREGUNTAS: " + preguntas.size());
        Pregunta pregunta = preguntas.get(position);
        Log.i(ConstantClass.TAG, "PREGUNTA: " + pregunta.toString());

        holder.pregunta = pregunta;
        holder.fraccion.setText((position + 1) + "/" + preguntas.size());
        holder.enunciado.setText(pregunta.pregunta);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.preguntas.size();
    }
}