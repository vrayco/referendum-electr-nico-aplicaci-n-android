package me.rayco.referendumelectronico;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by rayco on 5/4/15.
 */
public class Red {

    Context context = null;

    public Red (Context _context) {
        context = _context;
    }

    public boolean redActiva()
    {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();

        return i != null && i.isConnected() && i.isAvailable();
    }
}
