package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by rayco on 4/4/15.
 */
public class Registro {
    GoogleCloudMessaging gcm;
    Context context;

    public Registro(Context _context) {
        context = _context;
        gcm = GoogleCloudMessaging.getInstance(context);
    }

    public String checkRegisterId() {

        String regid = getRegistrationId(context);

        return regid;
    }

    public JSONObject IniciarRegistro(String dni) {

        String regid = getRegistrationId(context);

        if (regid.isEmpty())
            return registerInBackground(dni);
        else
            Log.i(ConstantClass.TAG, "REGID is NOT empty, no register execute");

        return null;
    }

    public boolean sendCodigo2ToBackend(String codigo2) throws IOException {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_VALIDAR_CODIGO2);

        final SharedPreferences prefs = getPreferences(context);
        String dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("dni", dni);
            jsonobj.put("codigo_2", codigo2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = new StringEntity(jsonobj.toString());
        httpost.setEntity(se);

        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        if(httpResponse.getStatusLine().getStatusCode() == 200)
            return true;
        else if(httpResponse.getStatusLine().getStatusCode() == 500)
            Log.i(ConstantClass.TAG, "El servidor tiene un error");

        return false;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        String registrationId = prefs.getString(ConstantClass.PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(ConstantClass.TAG, "La aplicación no tiene registrationId asignado.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(ConstantClass.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(ConstantClass.TAG, "App version changed.");
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(ConstantClass.PROPERTY_REG_ID);
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    private JSONObject registerInBackground(String dni) {
        JSONObject resultado  = null;
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(context);
            }

            String regid = null;
            regid = gcm.register(ConstantClass.SENDER_ID);
            Log.i(ConstantClass.TAG, "REGID="+regid);

            resultado = sendRegistrationIdToBackend(dni, regid);

            int code = resultado.getInt("codigo");

            if(code == 200)
                storeRegistrationId(context, regid, dni);



        }catch (IOException ex) {
            ex.printStackTrace();
            try {
                resultado = new JSONObject();
                resultado.put("codigo", 408);
                JSONObject contenido = new JSONObject();
                contenido.put("message", "No ha sido posible conectar con el servidor");
                resultado.put("contenido", contenido);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
                throw new RuntimeException("Error :" + e.getMessage());
        }

        return resultado;
    }

    private void storeRegistrationId(Context context, String regId, String dni) {
        final SharedPreferences prefs = getPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ConstantClass.PROPERTY_REG_ID, regId);
        editor.putString(ConstantClass.PROPERTY_DNI, dni);
        editor.putInt(ConstantClass.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private JSONObject sendRegistrationIdToBackend(String dni, String regId) throws IOException {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_INICIAR_SOLICITUD);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("dni", dni);
            jsonobj.put("sistema_operativo", "ANDROID");
            jsonobj.put("registration_id", regId);
            jsonobj.put("app_version", getAppVersion(context));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = new StringEntity(jsonobj.toString());
        httpost.setEntity(se);

        //sets a request header so the page receving the request
        //will know what to do with it
        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        JSONObject content = null;
        try {
            String resp_body = EntityUtils.toString(httpResponse.getEntity());
            content = new JSONObject(resp_body);
        } catch (JSONException e) {
            Log.i(ConstantClass.TAG, "Problema creando el JSONObject para la respuesta.");
            e.printStackTrace();
        }

        if(httpResponse.getStatusLine().getStatusCode() == 412)
            Log.i(ConstantClass.TAG, "La solicitud ya existe en el sistema");
        else if(httpResponse.getStatusLine().getStatusCode() == 409)
            Log.i(ConstantClass.TAG, "El servidor no pudo enviar vía GCM el código 1");
        else if(httpResponse.getStatusLine().getStatusCode() == 500)
            Log.i(ConstantClass.TAG, "El servidor tiene un error");

        JSONObject result = new JSONObject();

        try {
            result.put("codigo", httpResponse.getStatusLine().getStatusCode());
            result.put("contenido",content);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

//    private JSONObject confirmarClavePublicaToBackend(String dni, String publicKey) throws IOException {
//        //instantiates httpclient to make request
//        DefaultHttpClient httpclient = new DefaultHttpClient();
//
//        //url with the post data
//        HttpPost httpost = new HttpPost(ConstantClass.API_ENTREGAR_CLAVE_PUBLICA);
//
//        JSONObject jsonobj = new JSONObject();
//        try {
//            jsonobj.put("dni", dni);
//            jsonobj.put("clave_publica", publicKey);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        StringEntity se = new StringEntity(jsonobj.toString());
//        httpost.setEntity(se);
//
//        //sets a request header so the page receving the request
//        //will know what to do with it
//        httpost.setHeader("Accept", "application/json");
//        httpost.setHeader("Content-type", "application/json;charset=UTF-8");
//
//        HttpResponse httpResponse = httpclient.execute(httpost);
//
//        JSONObject content = null;
//        try {
//            String resp_body = EntityUtils.toString(httpResponse.getEntity());
//            content = new JSONObject(resp_body);
//        } catch (JSONException e) {
//            Log.i(ConstantClass.TAG, "Problema creando el JSONObject para la respuesta.");
//            e.printStackTrace();
//        }
//
//        if(httpResponse.getStatusLine().getStatusCode() == 412)
//            Log.i(ConstantClass.TAG, "La solicitud ya existe en el sistema");
//        else if(httpResponse.getStatusLine().getStatusCode() == 409)
//            Log.i(ConstantClass.TAG, "El servidor no pudo enviar vía GCM el código 1");
//        else if(httpResponse.getStatusLine().getStatusCode() == 500)
//            Log.i(ConstantClass.TAG, "El servidor tiene un error");
//
//        JSONObject result = new JSONObject();
//
//        try {
//            result.put("codigo", httpResponse.getStatusLine().getStatusCode());
//            result.put("contenido",content);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }

}
