package me.rayco.referendumelectronico;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by rayco on 3/4/15.
 */
public class RegistroService extends IntentService {

    GoogleCloudMessaging gcm;
    Context context;
    String dni;
    String regid;

    public RegistroService() {
        super("RegistroService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        context = getApplicationContext();

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();

        if (intent != null) {
            final SharedPreferences prefs = getPreferences(context);
            regid = prefs.getString(ConstantClass.PROPERTY_REG_ID, "");
            dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
            final String action = intent.getAction();
            if (ConstantClass.ACTION_DEVOLVER_CODIGO1.equals(action)) {
                final String codigo1 = intent.getStringExtra(ConstantClass.EXTRA_CODIGO1);
                handleActionDevolverCodigo1(codigo1, regid);
            } else if (ConstantClass.ACTION_CONFIRMAR_CLAVE_PUBLICA.equals(action)) {
                final String codigo_cifrado = intent.getStringExtra(ConstantClass.EXTRA_CODIGO_CIFRADO);
                handleActionConfirmarClavePublica(codigo_cifrado);
            } else if (ConstantClass.ACTION_ENTREGAR_CLAVE_PUBLICA.equals(action)) {
                handleActionEntregarClavePublica();
            }
        }

        wakeLock.release();
    }

    private void handleActionConfirmarClavePublica(String codigo_cifrado) {


        // Obtengo la clave privada
        Rsa rsa = new Rsa();
        PrivateKey privateKey = rsa.getPrivateKeyAndroid();

        // Descifro el código recibido con la clave publica.
        String codigo = rsa.privateDecrypt(codigo_cifrado, privateKey);
        Log.i(ConstantClass.TAG, "Codigo descifrado: " + codigo);

        boolean exito = false;
        while(!exito) {
            try {
                exito = confirmarClavePublicaToBackend(dni, codigo);
                Log.i(ConstantClass.TAG, "Entregando el codigo, resultado: " + exito);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Dejo constancia de que el proceso de entrega de la clave pública está abierto
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ConstantClass.PROPERTY_ENTREGA_CLAVE_PUBLICA_ABIERTO);
        editor.commit();

        // Salto a StartActivity
        Intent intent = new Intent(context, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    private void handleActionEntregarClavePublica() {
        Log.i(ConstantClass.TAG, "Genera par de claves y las entregamos al servidor");
        // Genero la clave RSA de la aplicacion
        Rsa rsa = new Rsa();
        Log.i(ConstantClass.TAG, "Genero key RSA");
        if(rsa.generarKey(context)) {
            Log.i(ConstantClass.TAG, "Devuelvo el código 2");
            Registro registro = new Registro(context);

            String publicKey = rsa.getPublicKeyAndroidPem();

            boolean exito = false;
            while(!exito) {
                try {
                    exito = entregarClavePublicaToBackend(dni, regid, publicKey);
                    Log.i(ConstantClass.TAG, "Entregando clave pública, resultado: " + exito);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Dejo constancia de que el proceso de entrega de la clave pública está abierto
            final SharedPreferences prefs = getPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(ConstantClass.PROPERTY_ENTREGA_CLAVE_PUBLICA_ABIERTO, true);
            editor.commit();

        } else {
            Log.i(ConstantClass.TAG, "Problema generando par de claves.");
        }
    }

    private void handleActionDevolverCodigo1(String codigo1, String regid) {
        boolean exito = false;
        while(!exito) {
            try {
                exito = sendCodigo1ToBackend(codigo1, regid);
                Log.i(ConstantClass.TAG, "Devolviedo codigo 1, resultado: " + exito);

                break;  // Hay que poner un temporizador para limitar el envío, por ahora rompo bucle. Porque en el servidor la acción lleva el envio de un sms

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Cierro la fase 1 y abro la fase 2 de la solitud
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_1);
        editor.putBoolean(ConstantClass.PROPERTY_SOLICITUD_REGISTRO_FASE_2, true);
        editor.commit();
    }

    private SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    private boolean sendCodigo1ToBackend(String codigo1, String regid) throws IOException {

        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_VALIDAR_CODIGO1);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("registration_id", regid);
            jsonobj.put("codigo_1", codigo1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = new StringEntity(jsonobj.toString());
        httpost.setEntity(se);

        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        if(httpResponse.getStatusLine().getStatusCode() == 200)
            return true;
        else if(httpResponse.getStatusLine().getStatusCode() == 400)
            Log.i(ConstantClass.TAG, "Bad request");
        else if(httpResponse.getStatusLine().getStatusCode() == 409)
            Log.i(ConstantClass.TAG, "El servidor no pudo enviar vía SMS el código 2");
        else if(httpResponse.getStatusLine().getStatusCode() == 500)
            Log.i(ConstantClass.TAG, "El servidor tiene un error");

        return false;
    }

    private boolean entregarClavePublicaToBackend(String dni, String regid, String publicKey) throws IOException {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_ENTREGAR_CLAVE_PUBLICA);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("dni", dni);
            jsonobj.put("registration_id", regid);
            jsonobj.put("clave_publica", publicKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = new StringEntity(jsonobj.toString());
        httpost.setEntity(se);

        //sets a request header so the page receving the request
        //will know what to do with it
        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        JSONObject content = null;
        try {
            String resp_body = EntityUtils.toString(httpResponse.getEntity());
            content = new JSONObject(resp_body);
        } catch (JSONException e) {
            Log.i(ConstantClass.TAG, "Problema creando el JSONObject para la respuesta.");
            e.printStackTrace();
        }

        if(httpResponse.getStatusLine().getStatusCode() == 400)
            Log.i(ConstantClass.TAG, "Hay un problema entregando la clave publica: " + content);

        if(httpResponse.getStatusLine().getStatusCode() == 200)
            return true;
        else
            return false;
    }

    private boolean confirmarClavePublicaToBackend(String dni, String codigo) throws IOException {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_CONFIRMAR_CLAVE_PUBLICA);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("dni", dni);
            jsonobj.put("registration_id", regid);
            jsonobj.put("codigo", codigo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = new StringEntity(jsonobj.toString());
        httpost.setEntity(se);

        //sets a request header so the page receving the request
        //will know what to do with it
        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        JSONObject content = null;
        try {
            String resp_body = EntityUtils.toString(httpResponse.getEntity());
            content = new JSONObject(resp_body);
        } catch (JSONException e) {
            Log.i(ConstantClass.TAG, "Problema creando el JSONObject para la respuesta.");
            e.printStackTrace();
        }

        if(httpResponse.getStatusLine().getStatusCode() == 400)
            Log.i(ConstantClass.TAG, "Hay un problema confirmando clave pública: " + content);

        if(httpResponse.getStatusLine().getStatusCode() == 200)
            return true;
        else
            return false;
    }

}
