package me.rayco.referendumelectronico;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.Respuesta;

public class RespuestasAdapter extends RecyclerView.Adapter<RespuestasAdapter.ViewHolder> {
    private List<Respuesta> respuestas;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Respuesta respuesta;
        public CheckBox respuestaCheckbox;


        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            respuestaCheckbox = (CheckBox) v.findViewById(R.id.respuesta_view_checkbox);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "Respuesta click");

            if(respuesta.seleccionado) {
                respuesta.seleccionado = false;
                respuesta.save();
            } else {

                List<Respuesta> todasLasRespuestas = respuesta.pregunta.respuestas();
                for (Respuesta r : todasLasRespuestas) {
                    r.seleccionado = false;
                    r.save();
                }

                respuesta.seleccionado = true;
                respuesta.save();
            }

            Intent intent = new Intent();
            intent.setAction(ConstantClass.ACTION_REFERENDUM_RESPUESTA);
            intent.putExtra("id_pregunta", respuesta.pregunta.iden);
            v.getContext().sendBroadcast(intent);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RespuestasAdapter(List<Respuesta> _respuestas) {
        respuestas = _respuestas;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RespuestasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.respuesta_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mView.setText(mDataset[position]);
        Respuesta r = respuestas.get(position);
        holder.respuesta = r;

        holder.respuestaCheckbox.setText(r.respuesta);
        if(r.seleccionado)
            holder.respuestaCheckbox.setChecked(true);
        else
            holder.respuestaCheckbox.setChecked(false);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return respuestas.size();
    }
}