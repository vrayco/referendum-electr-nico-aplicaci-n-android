package me.rayco.referendumelectronico;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

/**
 * Created by rayco on 8/4/15.
 */
public class Rsa {

    public Rsa() {}

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public boolean generarKey(Context context) {
        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date end = cal.getTime();

        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", ConstantClass.KEY_STORE);
            kpg.initialize(new KeyPairGeneratorSpec.Builder(context)
                    .setAlias(ConstantClass.APP_KEY_RSA)
                    .setStartDate(now)
                    .setEndDate(end)
                    .setSerialNumber(BigInteger.valueOf(1))
                    .setSubject(new X500Principal(ConstantClass.RSA_CN))
                    .build());

            KeyPair kp = kpg.generateKeyPair();

            Log.i(ConstantClass.TAG, "RSA (Private): " + kp.getPrivate().toString());
            Log.i(ConstantClass.TAG, "RSA (Public):  " + kp.getPublic().toString());

            return true;
        } catch (Exception e) {
            Log.i(ConstantClass.TAG, "Error generando key: " + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    // Devuelve la clave privada del dispositivo
    public PrivateKey getPrivateKeyAndroid() {
        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(ConstantClass.KEY_STORE);
            ks.load(null);
            KeyStore.Entry entry = ks.getEntry(ConstantClass.APP_KEY_RSA, null);
            if (entry != null) {
                if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                    Log.i(ConstantClass.TAG, "Not an instance of a PrivateKeyEntry");
                }
                PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
                Log.i(ConstantClass.TAG, "Clave privada: " + privateKey.toString());
                return privateKey;
            }
        } catch (Exception e) {
            Log.i(ConstantClass.TAG, e.getMessage());
        }
        return null;
    }

    public String getPublicKeyAndroidPem() {
        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(ConstantClass.KEY_STORE);
            ks.load(null);
            KeyStore.Entry entry2 = null;
            entry2 = ks.getEntry(ConstantClass.APP_KEY_RSA, null);
            // Codifico public key en Base64
            byte[] publicKeyEnconded = ((KeyStore.PrivateKeyEntry) entry2).getCertificate().getPublicKey().getEncoded();
            String publicKey = "-----BEGIN PUBLIC KEY-----\n" + Base64.encodeToString(publicKeyEnconded, Base64.DEFAULT) + "-----END PUBLIC KEY-----";

            return publicKey;

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }

        return null;
    }


    // Recibe un string con la clave pública en formato PEM y devuelve PublicKey
    public PublicKey getPublicKey(String strPublicKey) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException {

        // Elimino la cabecera y pie de la clave, si existen
        strPublicKey = strPublicKey.replace("-----BEGIN PUBLIC KEY-----\n","");
        strPublicKey = strPublicKey.replace("-----END PUBLIC KEY-----\n","");

        byte[] keyBytes = Base64.decode(strPublicKey.getBytes("utf-8"), Base64.DEFAULT);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = null;
        kf = KeyFactory.getInstance("RSA");

        return kf.generatePublic(spec);
    }

    public String publicEncrypt(String mensaje, PublicKey publicKey){
        byte[] encodedBytes = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE,  publicKey);
            encodedBytes = cipher.doFinal(mensaje.getBytes());

            return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public String privateEncrypt(String mensaje, PrivateKey privateKey){
        byte[] encodedBytes = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE,  privateKey);
            encodedBytes = cipher.doFinal(mensaje.getBytes());

            return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String publicDecrypt(String mensaje_cifrado, PublicKey publicKey)
    {
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE,  publicKey );
            decodedBytes = c.doFinal(Base64.decode(mensaje_cifrado, Base64.DEFAULT));
        } catch (Exception e) {
            Log.i(ConstantClass.TAG, e.getMessage());
            e.printStackTrace();
        }

        return new String(decodedBytes);
    }

    public String privateDecrypt(String mensaje_cifrado, PrivateKey privateKey)
    {
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE,  privateKey );
            decodedBytes = c.doFinal(Base64.decode(mensaje_cifrado, Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new String(decodedBytes);
    }

}
