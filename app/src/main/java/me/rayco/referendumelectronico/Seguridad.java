package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;

/**
 * Created by rayco on 5/4/15.
 */
public class Seguridad {

    String dni = null;
    String registrationId = null;
    Context context = null;

    public Seguridad(Context _context) {
        context = _context;
    }

    public void esUsuarioValido(String _dni) {

        dni = _dni;
        new ValidarUsuarioTask().execute();

    }

    public void getClavesPublicas()
    {
        new DescargarClavesPublicasSGTask().execute();
        new DescargarClavesPublicasSETask().execute();
    }

    private SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    private class ValidarUsuarioTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            return getValidacionUsuario(dni);
        }

        @Override
        protected void onPostExecute(String result) {
            String data = result;
            try {
                JSONObject json = new JSONObject(data);

                if(json.getString("validacion_usuario") != null) {
                    JSONObject validacion_usuario = new JSONObject(json.getString("validacion_usuario"));

                    if (!validacion_usuario.getBoolean("dni") || !validacion_usuario.getBoolean("telefono_movil")) {
                        // Cerramos la sesión
                        Log.i(ConstantClass.TAG, "Cerramos la sessión, borramos todos los datos");
                        final SharedPreferences prefs = context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.remove(ConstantClass.PROPERTY_REG_ID);
                        editor.remove(ConstantClass.PROPERTY_APP_VERSION);
                        editor.remove(ConstantClass.PROPERTY_DNI);
                        editor.remove(ConstantClass.PROPERTY_VALIDACION_COMPLETADA);
                        editor.commit();

                        // Paso a StartActivity
                        Intent intent = new Intent(context, StartActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public String getValidacionUsuario(String dni) {

        String url = String.format(ConstantClass.API_VALIDACION_USUARIO, dni);

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.i(ConstantClass.TAG, "Error consultado la validación del usuario.");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    public void esAplicacionValida(String _dni, String _registrationId) {
        dni = _dni;
        registrationId = _registrationId;

        new ValidarAplicacionTask().execute();
    }

    private class ValidarAplicacionTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            return getValidacionRSAAplicacion(dni, registrationId);
        }

        @Override
        protected void onPostExecute(String result) {
            String data = result;

            try {
                JSONObject json = new JSONObject(data);

                if(json.getString("validacion_aplicacion") != null) {
                    JSONObject validacion_aplicacion = new JSONObject(json.getString("validacion_aplicacion"));

                    // Aplicacion no válida: Esto pasa cuando se ha instalado y validado la aplicación en otro móvil para el mismo usuario
                    if(!validacion_aplicacion.getBoolean("app")) {

                        Log.i(ConstantClass.TAG, "Inicializamos la app ya que no es válida.");

                        // Eliminamos toda la información relativa al usuario
                        final SharedPreferences prefs = getPreferences(context);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.clear();
                        editor.commit();

                        // Vaciamos la bd
                        List<EventoElectoral> eventos = EventoElectoral.getAll();
                        for (EventoElectoral ev: eventos) {
                            ev.delete();
                        }

                        // Paso a StartActivity
                        Intent intent = new Intent(context, StartActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                    } else {
                        if (!validacion_aplicacion.getBoolean("clave_publica")) {

                            // Entregamos la clave pública
                            Intent mServiceIntent = new Intent(context, RegistroService.class);
                            mServiceIntent.setAction(ConstantClass.ACTION_ENTREGAR_CLAVE_PUBLICA);
                            //mServiceIntent.putExtra(ConstantClass.EXTRA_CLAVE_PUBLICA, publicKey);
                            context.startService(mServiceIntent);

                            // Paso a StartActivity
                            Intent intent = new Intent(context, WaitActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public String getValidacionRSAAplicacion(String dni, String registrationId) {

        String url = String.format(ConstantClass.API_VALIDACION_APP, dni, registrationId);

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.i(ConstantClass.TAG, "Error consultado la validación de la aplicación.");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    private class DescargarClavesPublicasSGTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            return getClavePublicaSistemaGeneral();
        }

        @Override
        protected void onPostExecute(String result) {
            String data = result;

            try {
                JSONObject json = new JSONObject(data);

                if(json.getString("public_key") != null) {
                    String pubKey = json.getString("public_key");

                    final SharedPreferences prefs = context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove(ConstantClass.PROPERTY_CLAVE_PUBLICA_SG);
                    editor.putString(ConstantClass.PROPERTY_CLAVE_PUBLICA_SG, pubKey);
                    editor.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private class DescargarClavesPublicasSETask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            return getClavePublicaSistemaElectoral();
        }

        @Override
        protected void onPostExecute(String result) {
            String data = result;

            try {
                JSONObject json = new JSONObject(data);

                if(json.getString("public_key") != null) {
                    String pubKey = json.getString("public_key");

                    final SharedPreferences prefs = context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove(ConstantClass.PROPERTY_CLAVE_PUBLICA_SE);
                    editor.putString(ConstantClass.PROPERTY_CLAVE_PUBLICA_SE, pubKey);
                    editor.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public String getClavePublicaSistemaGeneral() {

        String url = String.format(ConstantClass.API_SG_CLAVE_PUBLICA);

        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

            } else {
                Log.i(ConstantClass.TAG, "Error descargando la clave pública del sistema general.");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    public String getClavePublicaSistemaElectoral() {

        String url = String.format(ConstantClass.API_SE_CLAVE_PUBLICA);

        StringBuilder builder = new StringBuilder();
        HttpGet httpGet = new HttpGet(url);
        DefaultHttpClient client = new MyHttpClientSistemaElectoral(context);

        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.i(ConstantClass.TAG, "Error descargando la clave pública del sistema electoral. [CODE="+statusLine.getStatusCode()+"]");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }



}
