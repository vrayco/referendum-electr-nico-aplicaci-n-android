package me.rayco.referendumelectronico;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.activeandroid.ActiveAndroid;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.Date;
import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;


public class StartActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        //ActiveAndroid.dispose();
        ActiveAndroid.initialize(this);
        Log.i(ConstantClass.TAG, "En startActivity");

        //File cacheDir = StorageUtils.getCacheDirectory(context);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this.getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {

            // Comprueba si se ha registrado la app ó si hay que cambiar el regid
            Registro registro = new Registro(getApplicationContext());
            String registrationId = registro.checkRegisterId();

            //String registrationId = prefs.getString(ConstantClass.PROPERTY_REG_ID, "");
            if (registrationId.isEmpty()) {  // Si es vacío, inicio el proceso de registro en el servidor
                // Cambio a DniActivity
                Log.i(ConstantClass.TAG, "La aplicación no está registrada en el servidor.");
                Intent intent = new Intent(this, GetDniActivity.class);
                startActivity(intent);

            } else {
                final SharedPreferences prefs = getPreferences(getApplicationContext());
                Boolean validacionCompletada = prefs.getBoolean(ConstantClass.PROPERTY_VALIDACION_COMPLETADA, false);
                if (validacionCompletada == false) {
                    // Falta el ultimo paso de la validación, recibir el codigo2 y devolverlo.
                    Intent intent = new Intent(this, GetCodigoSmsActivity.class);
                    startActivity(intent);
                } else {
                    String dni = prefs.getString(ConstantClass.PROPERTY_DNI, "");
                    Seguridad seguridad = new Seguridad(this.getApplicationContext());
                    seguridad.esUsuarioValido(dni);

                    // Comprueba que el servidor tiene la clave pública de la app.
                    // Sino la tiene genera una, y la entrega.
                    seguridad.esAplicacionValida(dni,registrationId);

                    // Descargamos las claves publicas de los sistemas (General y Electoral)
                    seguridad.getClavesPublicas();

                    // La aplicación está validada correctamente, pasamos a la funcionalidad
                    Intent intent = new Intent(this, EventosActivity.class);
                    startActivity(intent);
                    //Intent intent = new Intent(this, MainActivity.class);
                    //startActivity(intent);
                }
            }
        } else {
            Log.i(ConstantClass.TAG, "No valid Google Play Services APK found.");
        }
    }

    protected void onResume() {
        super.onResume();
        // Check device for Play Services APK.
        checkPlayServices();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        ConstantClass.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(ConstantClass.TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }
}
