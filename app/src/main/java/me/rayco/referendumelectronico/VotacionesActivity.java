package me.rayco.referendumelectronico;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;


public class VotacionesActivity extends ActionBarActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Integer id_evento = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_votaciones);

        mRecyclerView = (RecyclerView) findViewById(R.id.votaciones_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        this.id_evento = intent.getIntExtra("id_evento", -1);
        EventoElectoral evento = EventoElectoral.get(id_evento);

        //setTitle(evento.nombre);

        List<VotacionListasAbiertas> listasAbiertas = evento.VotacionesListasAbiertas();
        List<VotacionListasCerradas> listasCerradas = evento.VotacionesListasCerradas();
        List<VotacionReferendum> referendums = evento.VotacionesReferendums();

        mAdapter = new VotacionesAdapter(this, listasAbiertas, listasCerradas, referendums);
        mRecyclerView.setAdapter(mAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            setTitle(R.string.app_name);
            ab.setSubtitle(evento.nombre);
        } else {
            setTitle(evento.nombre);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_listas, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public void Votar(View view) {
        Intent intent3 = new Intent(this, ConfirmarVotacionActivity.class);
        intent3.putExtra("id_evento", this.id_evento);
        this.startActivity(intent3);
    }
}
