package me.rayco.referendumelectronico;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;

public class VotacionesAdapter extends RecyclerView.Adapter<VotacionesAdapter.ViewHolder> {
    private Context context;
    private List<VotacionListasAbiertas> listasAbiertas;
    private List<VotacionListasCerradas> listasCerradas;
    private List<VotacionReferendum> referendums;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public int idVotacion;
        public String tipoVotacion;

        public TextView firstLine;
        public TextView secondLine;
        public ImageView logotipo;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            firstLine = (TextView) v.findViewById(R.id.firstLine);
            secondLine = (TextView) v.findViewById(R.id.secondLine);
            logotipo = (ImageView) v.findViewById(R.id.iconEstado);
        }

        @Override
        public void onClick(View v) {
            Log.i(ConstantClass.TAG, "onClick en votacion " + getPosition());
            if(tipoVotacion.equals("VotacionListasReferendum")) {
                Intent intent = new Intent(v.getContext(), PreguntasActivity.class);
                intent.putExtra("id_votacion", idVotacion);
                v.getContext().startActivity(intent);
            } else {
                Intent intent = new Intent(v.getContext(), ListasActivity.class);
                intent.putExtra("id_votacion", idVotacion);
                intent.putExtra("tipo_votacion", tipoVotacion);
                v.getContext().startActivity(intent);
            }

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public VotacionesAdapter(Context _context, List<VotacionListasAbiertas> _listasAbiertas, List<VotacionListasCerradas> _listasCerradas, List<VotacionReferendum> _referendum) {
        this.context = _context;
        this.listasAbiertas = _listasAbiertas;
        this.listasCerradas = _listasCerradas;
        this.referendums = _referendum;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public VotacionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.votacion_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mView.setText(mDataset[position]);
        int index = -1;
        int tipo = -1;
        if(this.listasAbiertas.size() > 0 && position < this.listasAbiertas.size()) {
            index = position;
            tipo = 1;
        } else if(this.listasCerradas.size() > 0 && (this.listasAbiertas.size() <= position && position < (this.listasAbiertas.size() + this.listasCerradas.size()))) {
            index = position - this.listasAbiertas.size();
            tipo = 2;
        } else {
            index = position - this.listasAbiertas.size() - this.listasCerradas.size();
            tipo = 3;
        }

        Log.i(ConstantClass.TAG, "index: " + index + " tipo: " + tipo);

        int idVotacion = -1;
        String titulo = "";
        String subtitulo = "";
        switch (tipo) {
            case 1:
                VotacionListasAbiertas v1 = this.listasAbiertas.get(index);
                if(v1.logotipoURL != null) {
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + v1.logotipoURL, holder.logotipo);
                }
                holder.idVotacion = v1.iden;
                holder.tipoVotacion = "VotacionListasAbiertas";
                titulo = v1.eventoElectoral.nombre;
                subtitulo = v1.nombre;
                break;
            case 2:
                VotacionListasCerradas v2 = this.listasCerradas.get(index);
                if(v2.logotipoURL != null) {
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + v2.logotipoURL, holder.logotipo);
                }
                holder.idVotacion = v2.iden;
                holder.tipoVotacion = "VotacionListasCerradas";
                titulo = v2.eventoElectoral.nombre;
                subtitulo = v2.nombre;
                break;
            case 3:
                VotacionReferendum v3 = this.referendums.get(index);
                holder.logotipo.setImageResource(R.drawable.referendum);
                if(v3.logotipoURL != null) {
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.displayImage(ConstantClass.RESOURCES_URL + "/" + v3.logotipoURL, holder.logotipo);
                }
                holder.idVotacion = v3.iden;
                holder.tipoVotacion = "VotacionListasReferendum";
                titulo = v3.eventoElectoral.nombre;
                subtitulo = v3.nombre;
                break;
        }

        holder.firstLine.setText(titulo);
        holder.secondLine.setText(subtitulo);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.listasAbiertas.size() + this.listasCerradas.size() + this.referendums.size();
    }
}