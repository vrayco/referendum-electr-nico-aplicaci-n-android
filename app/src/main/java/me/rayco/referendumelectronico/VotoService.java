package me.rayco.referendumelectronico;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.games.event.Event;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.util.Date;

import me.rayco.referendumelectronico.model.EventoElectoral;

/**
 * Created by rayco on 3/4/15.
 */
public class VotoService extends IntentService {

    Context context;

    String codigo = null;
    String l1 = null;
    Integer eventoId = null;
    EventoElectoral evento = null;

    public VotoService() {
        super("VotoService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        context = getApplicationContext();

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag2");
        wakeLock.acquire();

        if (intent != null) {
            final SharedPreferences prefs = getPreferences(context);
            final String action = intent.getAction();
            if (ConstantClass.ACTION_VALIDAR_VOTO.equals(action)) {
                this.codigo = intent.getStringExtra(ConstantClass.EXTRA_CODIGO_VALIDACION);
                this.l1 = intent.getStringExtra(ConstantClass.EXTRA_L1);
                this.eventoId = intent.getIntExtra(ConstantClass.EXTRA_EVENTO_ID, -1);
                this.evento = EventoElectoral.get(eventoId);
                if(this.evento != null &&  l1.equals(this.evento.l1)) {
                    this.evento.l1Validado = true;
                    this.evento.save();
                    handleActionDevolverCodigoValidacion(codigo);
                }
            }
        }

        wakeLock.release();
    }

    private void handleActionDevolverCodigoValidacion(String codigo_cifrado) {

        // Obtengo la clave privada
        Rsa rsa = new Rsa();
        PrivateKey privateKey = rsa.getPrivateKeyAndroid();

        // Descifro el código recibido con la clave publica.
        String codigo = rsa.privateDecrypt(codigo_cifrado, privateKey);
        Log.i(ConstantClass.TAG, "Codigo descifrado: " + codigo);

        String data = "";
        while(data.length() == 0) {
            try {
                data = devolverCodigoValidacionToBackend(codigo);
                Log.i(ConstantClass.TAG, "Entregando el codigo, resultado: " + data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            JSONObject object1 = new JSONObject(data);
            Log.i(ConstantClass.TAG, "DATA: " + object1.toString());
            JSONObject object2 = object1.getJSONObject("data");
            JSONObject object3 = object2.getJSONObject("resguardo_voto");
            this.evento.l1Cifrado = object3.getString("l1_cifrado");
            this.evento.votoEmitido = new Date();
            this.evento.save();

            Log.i(ConstantClass.TAG, "VOTO EMITIDO: " + this.evento.votoEmitido.toString());
            Log.i(ConstantClass.TAG, "L1 CIFRADO: " + this.evento.l1Cifrado);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // TERMINO EL ESTADO DE ENVIO DEL VOTO
        final SharedPreferences prefs = getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ConstantClass.PROPERTY_VOTO_ENVIADO);
        editor.commit();

        Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notifications);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.votacion)
                        .setContentTitle("GRACIAS POR PARTICIPAR")
                        .setContentText("Tu voto ha sido validado")
                        .setSound(alarmSound)
                        .setLights(Color.BLUE, 3000, 3000)
                        .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

        Intent resultIntent = new Intent(this, EventoActivity.class);
        resultIntent.putExtra("id_evento", evento.iden);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        int mNotificationId = ConstantClass.NOTIFICACION_VOTO_VALIDADO;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    private SharedPreferences getPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    private String devolverCodigoValidacionToBackend(String codigo) throws IOException
    {
        StringBuilder builder = new StringBuilder();
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(ConstantClass.API_VALIDAR_VOTO);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("codigo", codigo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity se = new StringEntity(jsonobj.toString());
        httpost.setEntity(se);

        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json;charset=UTF-8");

        HttpResponse httpResponse = httpclient.execute(httpost);

        if(httpResponse.getStatusLine().getStatusCode() == 200) {

            HttpEntity entity = httpResponse.getEntity();
            InputStream content = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        else if(httpResponse.getStatusLine().getStatusCode() == 400)
            Log.i(ConstantClass.TAG, "Bad request");
        else if(httpResponse.getStatusLine().getStatusCode() == 406)
            Log.i(ConstantClass.TAG, "El proceso electoral no está abierto.");

        return builder.toString();
    }

}
