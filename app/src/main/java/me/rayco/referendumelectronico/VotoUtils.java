package me.rayco.referendumelectronico;

import android.content.ContentProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import me.rayco.referendumelectronico.model.Candidato;
import me.rayco.referendumelectronico.model.EventoElectoral;
import me.rayco.referendumelectronico.model.Lista;
import me.rayco.referendumelectronico.model.Pregunta;
import me.rayco.referendumelectronico.model.Respuesta;
import me.rayco.referendumelectronico.model.VotacionListasAbiertas;
import me.rayco.referendumelectronico.model.VotacionListasCerradas;
import me.rayco.referendumelectronico.model.VotacionReferendum;

/**
 * Created by rayco on 17/6/16.
 */
public class VotoUtils {

    private EventoElectoral eventoElectoral = null;
    private JSONObject voto = null;
    private Context context = null;

    private String pubkeySistemaElectoral = null;

    public VotoUtils(Context _context, EventoElectoral _eventoElectoral) {
        this.eventoElectoral = _eventoElectoral;
        this.context = _context;
    }

    public Boolean generar() throws JSONException {

        // Información general del evento
        JSONObject data = new JSONObject();
        data.put("evento_electoral", this.eventoElectoral.iden);

        // REFERENDUMS
        List<VotacionReferendum> referendums = this.eventoElectoral.VotacionesReferendums();

        JSONArray vReferendums = new JSONArray();
        for(VotacionReferendum v : referendums) {
            JSONObject temp = new JSONObject();
            temp.put("votacion_id", v.iden);
            List<Pregunta> preguntasList = v.Preguntas();
            JSONObject preguntas = new JSONObject();
            for(Pregunta p : preguntasList)
            {
                JSONArray a = new JSONArray();
                List<Respuesta> respuestas = p.respuestasSeleccinadas();
                for(Respuesta r: respuestas)
                    a.put(r.iden);

                preguntas.put(p.iden.toString(),a);
            }
            temp.put("preguntas", preguntas);
            vReferendums.put(temp);
        }

        data.put("votacion_referendums",vReferendums);

        // LISTAS CERRADAS
        List<VotacionListasCerradas> votacionListasCerradas = this.eventoElectoral.VotacionesListasCerradas();
        JSONArray vCerradas = new JSONArray();
        for(VotacionListasCerradas v : votacionListasCerradas) {
            JSONObject temp = new JSONObject();
            temp.put("votacion_id",v.iden);

            List<Lista> listasLists = v.ListasSeleccionadas();
            JSONArray listas = new JSONArray();
            for(Lista l : listasLists)
            {
                listas.put(l.iden);
            }
            temp.put("listas", listas);

            vCerradas.put(temp);
        }

        data.put("votacion_listas_cerradas",vCerradas);

        // LISTAS ABIERTOS
        List<VotacionListasAbiertas> votacionListasAbiertas = this.eventoElectoral.VotacionesListasAbiertas();
        JSONArray vAbiertas = new JSONArray();
        for(VotacionListasAbiertas v : votacionListasAbiertas) {
            JSONObject temp = new JSONObject();
            temp.put("votacion_id",v.iden);

            List<Candidato> candidatosList = v.CandidatosSeleccionadas();
            JSONArray candidatos = new JSONArray();
            for(Candidato c : candidatosList)
            {
                candidatos.put(c.iden);
            }
            temp.put("candidatos", candidatos);

            vAbiertas.put(temp);
        }

        data.put("votacion_listas_abiertas",vAbiertas);

        try {
            Rsa rsa = new Rsa();
            MCrypt mcrypt = new MCrypt();

            // Cifrado simétrico del voto, generamos aleatoriamente contraseña e iv
            String iv = mcrypt.setRandomIV();
            String key = mcrypt.setRandomKey();

            String votoCifrado = MCrypt.bytesToHex( mcrypt.encrypt(data.toString()) );

            // Cifrado asimetrico de key, con la clave pública del evento electoral
            PublicKey pubkey = rsa.getPublicKey(eventoElectoral.pubKey);
            String keyCifrado = rsa.publicEncrypt(key, pubkey);

            Log.i(ConstantClass.TAG,"key="+key+" keyCif="+keyCifrado);
            JSONObject b = new JSONObject();
            b.put("iv",iv);
            b.put("key",keyCifrado);
            b.put("v",votoCifrado);

            // Descifro cv, ya que el Sistema General lo manda cifrado
            PrivateKey privKey = rsa.getPrivateKeyAndroid();
            String cvDescifrado = rsa.privateDecrypt(eventoElectoral.tokenVotacion, privKey);

            // Genero/refresco L1
            eventoElectoral.generateL1();
            eventoElectoral.save();

            this.voto = new JSONObject();
            this.voto.put("cv", cvDescifrado);
            this.voto.put("l1", eventoElectoral.l1);
            this.voto.put("v_prima", b);

            return true;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    // Devuelve el voto cifrado con la clave publica del Sistema Electoral
    public String getVotoCifrado()
    {
        // Obtengo la clave publica del SE
        final SharedPreferences prefs = context.getSharedPreferences(ConstantClass.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String pubKeyStr = prefs.getString(ConstantClass.PROPERTY_CLAVE_PUBLICA_SE, "");


        Rsa rsa = new Rsa();
        MCrypt mcrypt = new MCrypt();

        try {

            // Genera key e iv aleatorios
            String iv = mcrypt.setRandomIV();
            String key = mcrypt.setRandomKey();

            // Cifrado simétrico
            String votoCifrado = MCrypt.bytesToHex( mcrypt.encrypt(this.voto.toString()) );

            // Cifrado asimetrico de key, con la clave pública del Sistema Electoral
            PublicKey pubkey = rsa.getPublicKey(pubKeyStr);
            String keyCifrado = rsa.publicEncrypt(key, pubkey);

            JSONObject resultado = new JSONObject();
            resultado.put("iv", iv);
            resultado.put("key", keyCifrado);
            resultado.put("m", votoCifrado);

            return resultado.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


}
