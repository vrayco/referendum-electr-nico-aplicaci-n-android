package me.rayco.referendumelectronico;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;


public class WaitActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait);

        Log.i(ConstantClass.TAG, "En waitActivity");

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#415ba0"), PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public void onBackPressed() {
        Log.i(ConstantClass.TAG, "No se puede volver, si se esta en modo wait");
    }

}
