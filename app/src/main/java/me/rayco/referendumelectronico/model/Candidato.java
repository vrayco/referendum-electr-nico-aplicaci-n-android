package me.rayco.referendumelectronico.model;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

import me.rayco.referendumelectronico.ConstantClass;

@Table(name = "Candidato")
public class Candidato extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "nombre_completo")
    public String nombreCompleto;

    @Column(name = "foto_url")
    public String fotoURL;

    @Column(name = "posicion")
    public Integer posicion;

    @Column(name = "seccion_lista")
    public String seccionLista;

    @Column(name = "seleccionado")
    public Boolean seleccionado;

    @Column(name = "lista", onDelete = Column.ForeignKeyAction.CASCADE)
    public Lista lista;

    public Candidato() {
        super();
        this.seleccionado = false;
    }

    public String toString()
    {
        return "iden=" + this.iden + " | nombre=" + this.nombreCompleto + " | seleccionado = " + this.seleccionado +" | foto " + this.fotoURL + " | posicion=" + this.posicion + " | seccion_lista=" + this.seccionLista + " | id_lista: " + lista.iden;
    }

}
