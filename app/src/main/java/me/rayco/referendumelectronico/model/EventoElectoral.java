package me.rayco.referendumelectronico.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;

@Table(name = "EventoElectoral")
public class EventoElectoral extends Model {
    // If name is omitted, then the field name is used.
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "estado")
    public String estado;

    @Column(name = "logotipo_url")
    public String logotipoURL;

    @Column(name = "tablon_anuncios_url")
    public String tablonAnunciosUrl;

    @Column(name = "id_elector")
    public Integer id_elector;

    @Column(name = "token_votacion")
    public String tokenVotacion;

    @Column(name = "pub_key")
    public String pubKey;

    @Column(name = "pruebaDeVoto")
    public String pruebaDeVoto;

    @Column(name = "voto_emitido")
    public Date votoEmitido;

    @Column(name = "l1")
    public String l1;

    @Column(name = "l1_cifrado")
    public String l1Cifrado;

    @Column(name = "l1_validado")
    public Boolean l1Validado;

    public List<VotacionListasAbiertas> VotacionesListasAbiertas() {
        return getMany(VotacionListasAbiertas.class, "evento_electoral");
    }

    public List<VotacionListasCerradas> VotacionesListasCerradas() {
        return getMany(VotacionListasCerradas.class, "evento_electoral");
    }

    public List<VotacionReferendum> VotacionesReferendums() {
        return getMany(VotacionReferendum.class, "evento_electoral");
    }

    public EventoElectoral() { super(); }

    public EventoElectoral(Integer iden, String nombre, String descripcion) {
        super();
        this.iden       = iden;
        this.nombre     = nombre;
        this.l1Validado = false;
    }

    public static EventoElectoral get(Integer id_evento) {
        return new Select()
                .from(EventoElectoral.class)
                .where("iden = ?", id_evento)
                .executeSingle();
    }

    public static List<EventoElectoral> getAll() {
        return new Select()
                .from(EventoElectoral.class)
                .orderBy("nombre ASC")
                .execute();
    }

//    public static List<VotacionListasAbiertas> getVotacionesListasAbiertas(Integer id_evento) {
//        return new Select()
//                .from(VotacionListasAbiertas.class)
//                .where("evento_electoral = ?", id_evento)
//                .orderBy("nombre ASC")
//                .execute();
//    }
//
//    public static List<VotacionListasCerradas> getVotacionesListasCerradas(Integer id_evento) {
//        return new Select()
//                .from(VotacionListasCerradas.class)
//                .where("evento_electoral = ?", id_evento)
//                .orderBy("nombre ASC")
//                .execute();
//    }
//
//    public static List<VotacionReferendum> getVotacionesReferendums(Integer id_evento) {
//        return new Select()
//                .from(VotacionReferendum.class)
//                .where("evento_electoral = ?", id_evento)
//                .orderBy("nombre ASC")
//                .execute();
//    }

    public String toString() {
        return "iden= " + this.iden + " | nombre: " + this.nombre + " | descripcion: " + this.descripcion + " | logotipo " + this.logotipoURL + " | id_elector: " + id_elector + " | token_votacion: " + tokenVotacion + " | l1 = " + l1 + " | votoEmitido: " + votoEmitido;
    }

    public void generateL1()
    {
        // TODO, QUE METODO ESCOGER PARA CREAR L1?
//        Integer length = 32;
//        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
//        StringBuilder salt = new StringBuilder();
//        Random rnd = new Random();
//        while (salt.length() < length) {
//            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
//            salt.append(SALTCHARS.charAt(index));
//        }
//
//        this.l1 = salt.toString();

        SecureRandom random = new SecureRandom();
        this.l1 = new BigInteger(128, random).toString(32).toUpperCase();
    }
}
