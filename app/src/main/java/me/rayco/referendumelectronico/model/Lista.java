package me.rayco.referendumelectronico.model;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.nostra13.universalimageloader.utils.L;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.rayco.referendumelectronico.ConstantClass;

@Table(name = "Lista")
public class Lista extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "nombre_completo")
    public String nombreCompleto;

    @Column(name = "nombre_abreviado")
    public String nombreAbreviado;

    @Column(name = "path_logotipo")
    public String path_logotipo;

    @Column(name = "seleccionado")
    public Boolean seleccionado;

    @Column(name = "votacion_listas_cerradas", onDelete = Column.ForeignKeyAction.CASCADE)
    public VotacionListasCerradas votacionListasCerradas;

    @Column(name = "votacion_listas_abiertas", onDelete = Column.ForeignKeyAction.CASCADE)
    public VotacionListasAbiertas votacionListasAbiertas;

    public Lista() {
        super();
        this.seleccionado = false;
    }

    public static Lista get(Integer id) {
        return new Select()
                .from(Lista.class)
                .where("iden = ?", id)
                .executeSingle();
    }

    public List<Candidato> candidatos() {
        return getMany(Candidato.class, "lista");
    }

    public List<Candidato> candidatosOrdenados() {
        List<Candidato> all = getMany(Candidato.class, "lista");
        List<Candidato> titulares = new ArrayList<Candidato>();
        List<Candidato> suplentes = new ArrayList<Candidato>();
        List<Candidato> resultado = new ArrayList<Candidato>();

        for (Candidato c : all){
            if (c.seccionLista.equals("titular")) {
                titulares.add(c);
            } else if (c.seccionLista.equals("suplente")) {
                suplentes.add(c);
            }
        }

        // Ordeno por posición en la lista
        Collections.sort(titulares, new Comparator<Candidato>() {
            public int compare(Candidato c1, Candidato c2) {
                return c1.posicion < c2.posicion ? -1 : c1.posicion > c2.posicion ? 1 : 0;
            }
        });


        // Ordeno por posición en la lista
        Collections.sort(suplentes, new Comparator<Candidato>() {
            public int compare(Candidato c1, Candidato c2) {
                return c1.posicion < c2.posicion ? -1 : c1.posicion > c2.posicion ? 1 : 0;
            }
        });

        resultado.addAll(titulares);
        resultado.addAll(suplentes);

        return resultado;
    }

    public String toString()
    {
        return "LISTA(" + this.iden + "): nombre=" + this.nombreCompleto + " nombreAbr=" + this.nombreAbreviado + " seleccionado=" + seleccionado +" logotipo=" + this.path_logotipo + "votacion_lista_cerrada: " + votacionListasCerradas + " votacion_lista_abierta: " + votacionListasAbiertas;
    }

    public String getNombreVotacion()
    {
        if(this.votacionListasAbiertas != null)
            return this.votacionListasAbiertas.nombre;
        else if(this.votacionListasCerradas != null)
            return this.votacionListasCerradas.nombre;
        else
            return "";

    }

    public String getNombreEvento()
    {
        if(this.votacionListasAbiertas != null)
            return this.votacionListasAbiertas.eventoElectoral.nombre;
        else if(this.votacionListasCerradas != null)
            return this.votacionListasCerradas.eventoElectoral.nombre;
        else
            return "";
    }

    public Integer getNumeroCandidatosSeleccionados(Lista lista) {
        List<Candidato> candidatos = lista.candidatos();
        Integer candidatosSeleccionados = 0;
        for(Candidato c : candidatos)
            if(c.seleccionado)
                candidatosSeleccionados++;

        return candidatosSeleccionados;

    }
}
