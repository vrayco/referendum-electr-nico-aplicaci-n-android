package me.rayco.referendumelectronico.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Pregunta")
public class Pregunta extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "pregunta")
    public String pregunta;

    @Column(name = "orden")
    public Integer orden;

    // todo eliminar atributo
    @Column(name = "multirespuesta")
    public Boolean multirespuesta;

    @Column(name = "votacion_referendum", onDelete = Column.ForeignKeyAction.CASCADE)
    public VotacionReferendum votacionReferendum;

    public Pregunta() {
        super();
    }

    public String toString()
    {
        return "iden=" + this.iden + " | pregunta=" + this.pregunta + " | orden = " + this.orden +" | multirespuesta " + this.multirespuesta + " | referendum=" + this.votacionReferendum;
    }

    public static Pregunta get(Integer id) {
        return new Select()
                .from(Pregunta.class)
                .where("iden = ?", id)
                .executeSingle();
    }

    public List<Respuesta> respuestas() {
        return getMany(Respuesta.class, "pregunta");
    }

    public List<Respuesta> respuestasSeleccinadas()
    {
        List<Respuesta> resultado = new ArrayList<>();
        List<Respuesta> todas = this.respuestas();
        for(Respuesta r : todas)
            if(r.seleccionado)
                resultado.add(r);

        return resultado;
    }
}
