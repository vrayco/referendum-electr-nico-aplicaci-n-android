package me.rayco.referendumelectronico.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Respuesta")
public class Respuesta extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "respuesta")
    public String respuesta;

    @Column(name = "orden")
    public Integer orden;

    @Column(name = "posicion")
    public Integer posicion;

    @Column(name = "seleccionado")
    public Boolean seleccionado;

    @Column(name = "pregunta", onDelete = Column.ForeignKeyAction.CASCADE)
    public Pregunta pregunta;

    public Respuesta() {
        super();
        this.seleccionado = false;
    }

    public String toString()
    {
        return "iden=" + this.iden + " | respuesta=" + this.respuesta + " | seleccionado = " + this.seleccionado +" | orden " + this.orden + " | posicion=" + this.posicion + " | pregunta=" + this.pregunta;
    }

}
