package me.rayco.referendumelectronico.model;

import android.database.Cursor;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import me.rayco.referendumelectronico.ConstantClass;

@Table(name = "VotacionListasAbiertas")
public class VotacionListasAbiertas extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "logotipo_url")
    public String logotipoURL;

    @Column(name = "voto_en_blanco")
    public Boolean votoEnBlanco;

    @Column(name = "votos_por_elector")
    public Integer votosPorElector;

    @Column(name = "evento_electoral", onDelete = Column.ForeignKeyAction.CASCADE)
    public EventoElectoral eventoElectoral;

    public VotacionListasAbiertas() {
        super();
    }

    public List<Lista> Listas() {
        return getMany(Lista.class, "votacion_listas_abiertas");
    }

    public static VotacionListasAbiertas get(Integer id_votacion) {
        return new Select()
                .from(VotacionListasAbiertas.class)
                .where("iden = ?", id_votacion)
                .executeSingle();
    }

    public Integer getNumeroSeleccionados(VotacionListasAbiertas votacion) {

        List<Lista> listas = votacion.Listas();

        Integer candidatosSeleccionados = 0;
        for (Lista l : listas) {
            List<Candidato> candidatos = l.candidatos();
            for(Candidato c : candidatos)
                if(c.seleccionado)
                    candidatosSeleccionados++;
        }

        return candidatosSeleccionados;

    }

    public List<Candidato> CandidatosSeleccionadas()
    {
        List<Candidato> resultado = new ArrayList<>();
        List<Lista> listas = this.Listas();
        for(Lista l : listas) {
            List<Candidato> candidatos = l.candidatos();
            for(Candidato c : candidatos)
                if(c.seleccionado)
                    resultado.add(c);
        }

        return resultado;
    }

}
