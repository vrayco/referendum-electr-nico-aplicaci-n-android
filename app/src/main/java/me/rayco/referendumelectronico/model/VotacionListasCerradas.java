package me.rayco.referendumelectronico.model;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import me.rayco.referendumelectronico.ConstantClass;

@Table(name = "VotacionListasCerradas")
public class VotacionListasCerradas extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "logotipo_url")
    public String logotipoURL;

    @Column(name = "voto_en_blanco")
    public Boolean votoEnBlanco;

    @Column(name = "votos_por_elector")
    public Integer votosPorElector;

    @Column(name = "evento_electoral", onDelete = Column.ForeignKeyAction.CASCADE)
    public EventoElectoral eventoElectoral;

    public List<Lista> Listas() {
        return getMany(Lista.class, "votacion_listas_cerradas");
    }

    public List<Lista> ListasSeleccionadas() {
        List<Lista> listas = this.Listas();
        List<Lista> resultado = new ArrayList<>();
        for(Lista l : listas) {
            if(l.seleccionado)
                resultado.add(l);
        }

        return resultado;
    }

    public String toString() {
        return "iden= " + this.iden + "| Evento: " + eventoElectoral.iden + " | nombre: " + this.nombre + " | descripcion: " + " | logotipo " + this.logotipoURL + this.descripcion + " | blanco:" + this.votoEnBlanco + " | Votos por elector:" + this.votosPorElector;
    }

    public static VotacionListasCerradas get(Integer id_votacion) {
        return new Select()
                .from(VotacionListasCerradas.class)
                .where("iden = ?", id_votacion)
                .executeSingle();
    }
}
