package me.rayco.referendumelectronico.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "VotacionReferendum")
public class VotacionReferendum extends Model {
    @Column(name = "iden")
    public Integer iden;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "logotipo_url")
    public String logotipoURL;

    @Column(name = "voto_en_blanco")
    public Boolean votoEnBlanco;

    @Column(name = "evento_electoral", onDelete = Column.ForeignKeyAction.CASCADE)
    public EventoElectoral eventoElectoral;

    public VotacionReferendum() {
        super();
    }

    public static VotacionReferendum get(Integer id_votacion) {
        return new Select()
                .from(VotacionReferendum.class)
                .where("iden = ?", id_votacion)
                .executeSingle();
    }

    public List<Pregunta> Preguntas() {
        return getMany(Pregunta.class, "votacion_referendum");
    }
}
